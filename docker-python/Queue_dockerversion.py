import json
import requests
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings('ignore')
import os
import datetime
import jdatetime
jdatetime.set_locale('fa_IR')
import time
import statistics
import re
import json
from persiantools.jdatetime import JalaliDate
def convert_en_numbers(input_str):
    """
    Converts English numbers to Persian numbers
    :param input_str: String contains English numbers
    :return: New string with Persian numbers
    """
    mapping = {
        '0': '۰',
        '1': '۱',
        '2': '۲',
        '3': '۳',
        '4': '۴',
        '5': '۵',
        '6': '۶',
        '7': '۷',
        '8': '۸',
        '9': '۹',
        '.': '.',
    }
    return _multiple_replace(mapping, input_str)
def convert_en_characters(input_str):
    """
        Assumes that characters written with standard persian keyboard
        not windows arabic layout
    :param input_str: String contains English chars
    :return: New string with related characters on Persian standard keyboard
    """
    mapping = {
        'q': 'ض',
        'w': 'ص',
        'e': 'ث',
        'r': 'ق',
        't': 'ف',
        'y': 'غ',
        'u': 'ع',
        'i': 'ه',
        'o': 'خ',
        'p': 'ح',
        '[': 'ج',
        ']': 'چ',
        'a': 'ش',
        's': 'س',
        'd': 'ی',
        'f': 'ب',
        'g': 'ل',
        'h': 'ا',
        'j': 'ت',
        'k': 'ن',
        'l': 'م',
        ';': 'ک',
        "'": 'گ',
        'z': 'ظ',
        'x': 'ط',
        'c': 'ز',
        'v': 'ر',
        'b': 'ذ',
        'n': 'د',
        'm': 'پ',
        ',': 'و',
        '?': '؟',
    }
    return _multiple_replace(mapping, input_str)
def convert_ar_numbers(input_str):
    """
    Converts Arabic numbers to Persian numbers
    :param input_str: String contains Arabic numbers
    :return: New str and replaces arabic number with persian numbers
    """
    mapping = {
        '١': '۱',  # Arabic 1 is 0x661 and Persian one is 0x6f1
        '٢': '۲',  # More info https://goo.gl/SPiBtn
        '٣': '۳',
        '٤': '۴',
        '٥': '۵',
        '٦': '۶',
        '٧': '۷',
        '٨': '۸',
        '٩': '۹',
        '٠': '۰',
    }
    return _multiple_replace(mapping, input_str)
def convert_fa_numbers(input_str):
    """
    This function convert Persian numbers to English numbers.
    
    Keyword arguments:
    input_str -- It should be string
    Returns: English numbers
    """
    mapping = {
        '۰': '0',
        '۱': '1',
        '۲': '2',
        '۳': '3',
        '۴': '4',
        '۵': '5',
        '۶': '6',
        '۷': '7',
        '۸': '8',
        '۹': '9',
        '.': '.',
    }
    return _multiple_replace(mapping, input_str)
def convert_ar_characters(input_str):
    """
    Converts Arabic chars to related Persian unicode char
    :param input_str: String contains Arabic chars
    :return: New str with converted arabic chars
    """
    mapping = {
        'ك': 'ک',
        'دِ': 'د',
        'بِ': 'ب',
        'زِ': 'ز',
        'ذِ': 'ذ',
        'شِ': 'ش',
        'سِ': 'س',
        'ى': 'ی',
        'ي': 'ی'
    }
    return _multiple_replace(mapping, input_str)
def _multiple_replace(mapping, text):
    """
    Internal function for replace all mapping keys for a input string
    :param mapping: replacing mapping keys
    :param text: user input string
    :return: New string with converted mapping keys to values
    """
    pattern = "|".join(map(re.escape, mapping.keys()))
    return re.sub(pattern, lambda m: mapping[m.group()], str(text))
def market():
    url='http://www.tsetmc.com/tsev2/data/MarketWatchPlus.aspx'
    data = requests.get(url,timeout=15)
    content = data.content.decode('utf-8')

    parts = content.split('@')
    inst_price = parts[2].split(';')
    market = { }
    for item in inst_price:
        item=item.split(',')
        market[item[2]]= dict(id=item[0],ISIN=item[1],symbol=convert_ar_characters(item[2]),
                              name=item[3],first_price=item[5],close_price=item[6],
                              last_price=item[7],count=item[8],volume=item[9],
                              value=item[10],min_traded_price=item[11],
                              max_treaded_price=item[12],yesterday_price=item[13],
                              eps=item[14],base_volume=item[15],c2=item[16],
                              table_id=item[17],group_id=item[18],max_allowed_price=item[19],
                              min_allowed_price=item[20],type_of_symbol=item[22],
                              number_of_shares=item[21], Market_cap=int(item[21]) *int(item[6]) )
    return market
def indiv():
    count = 0
    while count <= 10:
        url = 'http://www.tsetmc.com/tsev2/data/ClientTypeAll.aspx'
        data = requests.get(url, timeout=10)
        content=data.content.decode('utf-8').split(";")
        if data.status_code != 200:
            count += 1
            time.sleep(0.5)
        elif data.status_code == 200:
            break
        elif count == 10:
            raise Exception('Loop Client!')
        else:
            time.sleep(1)
            pass
    all_data = pd.DataFrame(market()).T
    all_data = all_data[all_data['ISIN'].map(lambda x:not ( x.startswith('IRT')))]
    all_data = all_data[all_data['ISIN'].map(lambda x:not ( x.startswith('IRB')))]
    
    others = ['آ س پ' , 'جم پيلن', 'كي بي سي' , 'فن آوا' , 'انرژي3' , 'دتهران\u200c', 'هاي وب']
    for item in list(all_data.index):
        if item.isalpha() ==False:
            if item in others:
                continue
            all_data = all_data.drop(item, axis = 0)

    if True:
        all_data['num'] = [i for i in range(all_data.shape[0])]
        all_data = all_data.set_index('num')
    clienttype=[]
    for item in content:
        try:
            item=item.split(',')
            symbol = all_data[ all_data['id'] ==  (item[0]) ]['symbol'].iloc[0]
            clienttype.append(dict( id=item[0],Name = symbol,
                                       Individual_buy_count=int(item[1]),
                                          NonIndividual_buy_count=int(item[2]),
                                          Individual_buy_volume=int(item[3]),
                                          NonIndividual_buy_volume=int( item[4]) ,
                                          Individual_sell_count=int(item[5]),
                                          NonIndividual_sell_count=int(item[6]),
                                          Individual_sell_volume=int(item[7]),
                                          NonIndividual_sell_volume=int(item[8]),
                                         Value = float(all_data[all_data['symbol'] == symbol]['value'])))
        except:
            continue
    clients = pd.DataFrame(clienttype)
    clients.Name = clients.Name.map(lambda x: convert_ar_characters(x) )
    clients['VAL_hoghooghi_SELL'] = clients['Value'] * clients['NonIndividual_sell_volume'].astype(float) /\
    (clients['Individual_sell_volume'].astype(float) +clients['NonIndividual_sell_volume'].astype(float))

    clients['VAL_haghighi_BUY'] = clients['Value'] * clients['Individual_buy_volume'].astype(float) /\
    (clients['Individual_buy_volume'].astype(float) +clients['NonIndividual_buy_volume'].astype(float))
    clients['VAL_haghighi_SELL'] = clients['Value'] * clients['Individual_sell_volume'].astype(float) /\
    (clients['Individual_sell_volume'].astype(float) +clients['NonIndividual_sell_volume'].astype(float))
    clients['percapita_buy'] = clients['VAL_haghighi_BUY'] / clients['Individual_buy_count']
    clients['percapita_sell'] = clients['VAL_haghighi_SELL'] / clients['Individual_sell_count']
    clients['power'] = clients['percapita_buy'] / clients['percapita_sell']
    clients['VAL_net_haghigh'] = clients['VAL_haghighi_BUY'] - clients['VAL_haghighi_SELL']

    return clients 
def Market_with_askbid():
    count = 0 
    while count<15:
        url = 'http://www.tsetmc.com/tsev2/data/MarketWatchPlus.aspx?h=0&r=0'
        data = requests.get(url, timeout=15)
        content = data.content.decode('utf-8')
        parts = content.split('@')
        if data.status_code != 200 or len(content.split('@')[2])<400:
            count+=1
            time.sleep(1)
        if count ==15:
            raise Exception('ohoh')
        if data.status_code == 200 and len(content.split('@')[2]) > 400:
            break
    parts = content.split('@')
    inst_price = parts[2].split(';')
    market_me = {}
    # Add the Trade and other stuff to dataframe--------
    for item in inst_price:
        item=item.split(',')
        market_me[item[0]]= dict(id=item[0],ISIN=item[1],symbol=item[2],
                              name=item[3],first_price=float(item[5]),close_price=float(item[6]),
                              last_trade=float(item[7]),count=item[8],volume=float(item[9]),
                              value=float(item[10]),min_traded_price=float(item[11]),
                              max_treaded_price=float(item[12]),yesterday_price=int(item[13]),
                              table_id=item[17],group_id=item[18],max_allowed_price=float(item[19]),
                              min_allowed_price=float(item[20]),last_ret = (float(item[7]) - float(item[13]))/float(item[13]),
                                 ret = (float(item[6]) - float(item[13]))/float(item[13]),
                                number_of_shares=float(item[21]), Market_cap=int(item[21]) *int(item[6]))
    # Add the Ask-Bid price Vol tu dataframe --------
    for item in parts[3].split(';'):
        try:
            item=item.split(',')
            if item[1] == '1':
                market_me[item[0]]['ask_price'.format(item[1])]=  float(item[4])
                market_me[item[0]]['ask_vol'.format(item[1])]=  float(item[6])
                
                market_me[item[0]]['bid_price'.format(item[1])]=  float(item[5])
                market_me[item[0]]['bid_vol'.format(item[1])]=  float(item[7])

        except:
            pass
    df = pd.DataFrame(market_me).T
    df = df[df['ISIN'].map(lambda x:not ( x.startswith('IRT')))]
    df = df[df['ISIN'].map(lambda x: not( x.startswith('IRB')))]
    df = df[df['symbol'].map(lambda x: x.isalpha())]
    df.symbol = df.symbol.map(lambda x: convert_ar_characters(x) )
    df = df.set_index('symbol')
    return df
def readable(n):
    human_readable =''
    if float(n) < 0 : 
        human_readable += '-'
    n = abs(float(n))
    if n >= 1e7 and n<= 1e10 :
        round_number = n/1e7
        human_readable += '{:,.0f}{}'.format(round_number,   ' میلیون تومان ')
    elif n>1e10:
        round_number = n/1e10
        
        human_readable += '{:,.1f}{}'.format(round_number, ' میلیارد تومان ')
    else:
        round_number = n/10
        human_readable += '{:,.0f}{}'.format(round_number, 'تومان')
        
    return convert_en_numbers(human_readable)
def readable_tedad(n):
    human_readable =''
    if int(n) < 0 : 
        human_readable += '-'
    n = abs(int(n))
    if n >= 1e6 and n<= 1e9 :
        round_number = n/1e6
        human_readable += '{:,.1f}{}'.format(round_number,   ' میلیون ')
    elif n>1e9:
        round_number = n/1e9
        
        human_readable += '{:,.1f}{}'.format(round_number, ' میلیارد ')
    else:
        human_readable += '{:,}'.format(n)
    return convert_en_numbers(human_readable)
def average_month(id):
    counter = 0
    while counter < 10:
        url = 'http://members.tsetmc.com/tsev2/data/InstTradeHistory.aspx?i={}&Top=20&A=1'.format(id)
        data = requests.get(url, timeout=8, verify=False)
        content = data.content.decode('utf-8').split(';')
        if data.status_code == 200 and len(content) > 5:
            break
        else:
            counter += 1
    hist_vol = []
    for i in range(len(content) - 1):
        hist_vol.append(float(content[i].split('@')[-2]))
    mean = statistics.mean(hist_vol)
    return mean
def telegram_msg(msg , chat_id = "-1001470501669"):
    headers = {'Content-type': 'application/json'}
    payload = {"bot-name" : "hermes" , 
              "chat-id" : chat_id,
              "message" : msg,
              "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                      headers = headers,
                      data=json.dumps(payload))
    payload = {"bot-name" : "hermes" , 
              "chat-id" : '-1001453076404',
              "message" : msg,
              "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                      headers = headers,
                      data=json.dumps(payload))
    payload = {"bot-name" : "hermes" , 
          "chat-id" : '-1001279557836',
          "message" : msg,
          "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                  headers = headers,
                  data=json.dumps(payload))
    payload = {"bot-name" : "hermes" , 
          "chat-id" : '-1001437707813',
          "message" : msg,
          "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                  headers = headers,
                  data=json.dumps(payload))
    
    
    if r.status_code == 200:
        return True
    else: return False
def telegram_justme(msg , chat_id = "-1001470501669"):
    headers = {'Content-type': 'application/json'}
    payload = {"bot-name" : "hermes" , 
              "chat-id" : '80911655',
              "message" : msg,
              "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                      headers = headers,
                      data=json.dumps(payload))
def telegram_img( chat_id = "80911655"):
#     headers = {'Content-type': 'application/json'}
#     headers = {'Content-type': 'multipart/form-data'}
    payload = {"bot-name" : "hermes" , 
                  "chat-id" : chat_id,
                   "caption" : " this is test"}
    
    r = requests.post('http://178.62.251.62:8891/send-image', 
                      files = {'image': open(x.jpg,'rb')},
                          headers = headers,
                          data=json.dumps(payload))

    
    if r.status_code == 200:
        return True
    else: return False
def URL(id):
    if type(id) == str:
        URLp = 'http://www.tsetmc.com/Loader.aspx?ParTree=151311&i={}'.format(id)
    else: 
        id = str(id)
        URLp = 'http://www.tsetmc.com/Loader.aspx?ParTree=151311&i={}'.format(id)
    return '<a href="{}"> -{}</a>'.format(URLp, '(TSETMC)')
def after_market():
    cl =indiv()
    static = Market_with_askbid()
    static['bid1_price']= static['ask1'].map(lambda x: x['price'])
    static['bid1_vol']= static['ask1'].map(lambda x: x['vol'])
    static['ask1_price'] = static['bid1'].map(lambda x:x['price'])
    static['ask1_vol'] = static['bid1'].map(lambda x:x['vol'])
    static.drop('ask1',axis=1,inplace=True)
    static.drop('bid1',axis=1,inplace=True)
    static['symbol'] =static.index
    AVEGRAGE = pd.read_excel('Average_vol_symbols.xlsx')
    AVEGRAGE['id'] = AVEGRAGE['id'] .astype(str)
    AVEGRAGE= AVEGRAGE.drop('Unnamed: 0' , axis=1)
    static['Average_monthly_volume'] = np.nan
    morning = 1
    for id in list(static['id'][static['Average_monthly_volume'].isna()]):
        try:
            static['Average_monthly_volume'][static['id'] == id] = AVEGRAGE['Average_monthly_volume'][AVEGRAGE['id'] ==id].iloc[0]
        except:
                pass
    x = pd.merge(cl , static)
    idx = x['percapita_buy'].nlargest(10).index
    x['vol/vol_av']  = x['volume'].astype(float) / x['Average_monthly_volume'].astype(float)
    x.reset_index(drop=True, inplace=True)
    x['power'] = x['percapita_buy'] / x['percapita_sell']
    return x
def merged_data_client_hist(id):
    history  =  histoy_trade_adj(id)
    shakhes=  main_index()
    shakhes = shakhes.reset_index()
    shakhes.columns= ['date','indexx']
    shakhes=shakhes.iloc[::-1]
    client_per_share = get_clienttype_history(int(id) , 2000000)
    merged = pd.merge(history, client_per_share, on='date')
    merged = pd.merge(merged, shakhes)
#     merged=merged.iloc[::-1]

    merged['date'] = pd.to_datetime(merged['date'])
    merged.set_index('date' , inplace=True)
    merged['I_buy_sarane']  = merged['Individual_buy_value'].astype(float) / merged['Individual_buy_count']
    merged['I_sell_sarane']  = merged['Individual_sell_value'].astype(float) / merged['Individual_sell_count']
    merged['power'] = merged['I_buy_sarane'] / merged['I_sell_sarane']
    merged['Avg_Buy_Ind_per_capita'] = merged['I_buy_sarane'].rolling(30).mean()
    merged['Avg_vol_30'] = merged['volume'].rolling(30).mean()
    for i in range(30):
        if i == 0: 
            merged['Avg_Buy_Ind_per_capita'] = merged['I_buy_sarane'].iloc[0]
            merged['Avg_vol_30'] = 0
        else:
            merged['Avg_vol_30'] = merged['volume'].iloc[:i].mean()
            merged['Avg_Buy_Ind_per_capita'].iloc[i] = merged['I_buy_sarane'].iloc[:i].mean()
    merged['%Sell_Non_Ind'] = merged['NonIndividual_sell_value'] /( merged['NonIndividual_sell_value'] + merged['Individual_sell_value'])
    merged['%Buy_Ind'] = merged['Individual_buy_value'] \
    /( merged['Individual_buy_value'] + merged['NonIndividual_buy_value'])
    merged['Sarane_Buy_to_avg'] = merged['I_buy_sarane'] / merged['Avg_Buy_Ind_per_capita']
    merged['vol_to_vol30'] = merged['volume'] / merged['Avg_vol_30']
    merged['close_price'] = merged['close_price'].astype(float)
    merged['ret1D'] = merged['close_price'].pct_change().dropna()
    merged['ret30D_fwd'] = (merged['close_price'].shift(-30) - merged['close_price'])/merged['close_price']

    merged['ret1D_index'] = merged['indexx'].pct_change().dropna()
    merged['ret30D_index'] = (merged['indexx'].shift(-30) - merged['indexx'])/merged['indexx']

    
    merged=merged.iloc[::-1]
    for i in range(30):
        if i == 0 :
            merged['ret30D_index'].iloc[0] = merged['ret1D_index'].iloc[0]
        else:
            merged['ret30D_index'].iloc[i] = merged['ret1D_index'].iloc[:i+1].sum()
    for i in range(30):
        if i == 0 :
            merged['ret30D_fwd'].iloc[0] = merged['ret1D'].iloc[0]
        else:
            merged['ret30D_fwd'].iloc[i] = merged['ret1D'].iloc[:i+1].sum()
    merged['ret_wrt_index'] = merged['ret30D_fwd'] - 0.5* merged['ret30D_index']
    return merged
def Market_with_askbidzz():
    url='http://www.tsetmc.com/tsev2/data/MarketWatchPlus.aspx?h=0&r=0'
    data = requests.get(url, timeout=10)
    content = data.content.decode('utf-8')
    parts = content.split('@')
    inst_price = parts[2].split(';')
    market_me = {}
    # Add the Trade and other stuff to dataframe--------
    for item in inst_price:
        item=item.split(',')
        market_me[item[0]]= dict(id=item[0],ISIN=item[1],symbol=item[2],
                              name=item[3],first_price=item[5],close_price=item[6],
                              last_trade=float(item[7]),count=item[8],volume=item[9],
                              value=float(item[10]),min_traded_price=item[11],
                              max_treaded_price=item[12],yesterday_price=int(item[13]),
                              table_id=item[17],group_id=item[18],max_allowed_price=float(item[19]),
                              min_allowed_price=float(item[20]),
                                number_of_shares=item[21], Market_cap=int(item[21]) *int(item[6]))
    # Add the Ask-Bid price Vol tu dataframe --------
    for item in parts[3].split(';'):
        try:
            item=item.split(',')
            if True:
                market_me[item[0]]['ask{}_price'.format(item[1]) ]=  float(item[4])
                market_me[item[0]]['ask{}_vol'.format(item[1]) ]=  float(item[6])
                market_me[item[0]]['ask{}_val'.format(item[1]) ] =  float(item[6]) * float(item[4])
                market_me[item[0]]['bid{}_price'.format(item[1]) ]=  float(item[5])
                market_me[item[0]]['bid{}_vol'.format(item[1]) ]=  float(item[7])
                market_me[item[0]]['bid{}_val'.format(item[1]) ] =  float(item[5]) * float(item[7])
                

        except:
            pass
    df = pd.DataFrame(market_me).T
    df = df[df['ISIN'].map(lambda x:not ( x.startswith('IRT')))]
    df = df[df['ISIN'].map(lambda x: not( x.startswith('IRB')))]
    df = df[df['symbol'].map(lambda x: x.isalpha())]
    df = df.set_index('symbol')
    return df
def holiday_market():
    static = Market_with_askbid()
    time.sleep(5)
    static2 = Market_with_askbid()
    boolz = static['bid_vol'].sum() == static2['bid_vol'].sum()
    if boolz:
        holiday = 1
    else:
        holiday=0
    return holiday





holiday = 0
morning = 0
second_check_static = 0
each_iter = 0

while True:
    try:
        time_x = datetime.datetime.now()
        if time_x > (datetime.datetime.combine(time_x, datetime.time(8,50))) and    morning==0 and\
        time_x < (datetime.datetime.combine(time_x, datetime.time(12,31))):
            each_iter = 0
            AVEGRAGE = pd.read_excel('Average_vol_symbols.xlsx')
            AVEGRAGE= AVEGRAGE.drop('Unnamed: 0' , axis=1)
            AVEGRAGE['Average_monthly_volume'][AVEGRAGE['Average_monthly_volume'] <  500e3] =1e6
            AVEGRAGE['id'] = AVEGRAGE['id'].astype(str)
            historical = pd.read_excel('TSE_importan_indexes_edited 10102020.xlsx')
            historical= historical.rename(columns={'شاخص هرمس' : 'شاخص کل(با قیمت آخرین معامله)'})
            historical['id'] = historical['id'].astype(str)
            marketz = Market_with_askbid()
            merged = pd.merge(marketz, historical, on='id', how='left')
            merged = merged.sort_values('Market_cap')
            
            merged['سهم_های_بزرگ'] = 0
            merged['سهم_های_متوسط'] = 0
            merged['سهم_های_کوچک'] = 0

            merged = pd.merge(merged, AVEGRAGE, on='id', how='left' )
            merged['average_vol'] = merged['Average_monthly_volume'].copy()
            merged=merged.drop(['symbol_y','Average_monthly_volume','yesterday_price_y'], axis=1)

            merged.rename(columns={'symbol_x': 'symbol','yesterday_price_x':'yesterday_price'},inplace=True)
            merged= merged.loc[merged['yesterday_price'].dropna().index]

            merged['سهم_های_بزرگ'][merged['Market_cap'] > np.percentile(merged['Market_cap'], 85)] = 1
            merged['سهم_های_متوسط'][(merged['Market_cap'] < np.percentile(merged['Market_cap'], 85)) & \
                          (merged['Market_cap'] > np.percentile(merged['Market_cap'],40))] = 1
            merged['سهم_های_کوچک'][merged['Market_cap'] < np.percentile(merged['Market_cap'],40)] = 1
            merged = merged[merged['ISIN_x'].map(lambda x: not( x.startswith('IRR')))]
            merged.reset_index(drop=True, inplace=True)
            merged['bidask'] = 0
            merged['bidask'][merged['ask_price'] == merged['max_allowed_price']] = 1
            merged['bidask'][merged['bid_price'] == merged['min_allowed_price']] = -1
            merged['bidvol_adj'] = 0
            merged['askvol_adj'] = 0
            
            merged['bidvol_adj'] = np.where( ((merged['bid_price'] == merged['ask_price']) & (merged['bidask'] == -1)) , \
                                            merged['bid_vol'] - merged['ask_vol'] , 0)
            merged['bidvol_adj'] = np.where( ((merged['bid_price'] != merged['ask_price']) & (merged['bidask'] == -1)) , \
                                            merged['bid_vol'] , merged['bidvol_adj'])
            
            
            merged['askvol_adj'] = np.where( ((merged['bid_price'] == merged['ask_price']) & (merged['bidask'] == 1)) , \
                                            -merged['bid_vol'] + merged['ask_vol'] , 0)
            merged['askvol_adj'] = np.where( ((merged['bid_price'] != merged['ask_price']) & (merged['bidask'] == 1)) , \
                                            merged['ask_vol'] , merged['askvol_adj'])
            
            
            merged['bidvol_adj'] = merged['bidvol_adj'].apply(lambda x: 0 if x<0 else x)
            merged['askvol_adj'] = merged['askvol_adj'].apply(lambda x: 0 if x<0 else x)
            string ='#' +  'پیش_گشایش' +'\n'  + ' 🕋به نام خدا🕋' +\
            '\n'  +convert_en_numbers(jdatetime.datetime.now().strftime("%A, %d %b %Y")) + '\n'
            string += ' 🌀ارزش صفوف خرید و فروش در صنایع مختلف: 🏁 \n' 
            for i,col in enumerate(merged.columns[-20:-3]):
                collist=  list(merged['id'][merged[col] == 1])
                col_index = merged['id'].isin(collist)
                buyqueuevalue_index =  round((merged['askvol_adj'][col_index] *merged['ask_price'][col_index]).sum())
                sellqueuevalue_index =  round((merged['bidvol_adj'][col_index] *merged['bid_price'][col_index]).sum())
                if col =='شاخص کل(با قیمت آخرین معامله)':
                    col = ' کل بازار '
                if buyqueuevalue_index >sellqueuevalue_index :

                    
                    string += '✅🐃' +'<strong>{}</strong>'.format(col)+'\n' +' : صف خرید ' + \
                    readable(buyqueuevalue_index)+'\n'+ '  صف فروش ' + \
                    readable(sellqueuevalue_index) +'\n'
                else:
                      string += '⭕🐻' +'<strong>{}</strong>'.format(col)+'\n' +' : صف خرید ' + \
                    readable(buyqueuevalue_index)+'\n'+ '  صف فروش ' + \
                    readable(sellqueuevalue_index) +'\n'
            holiday = holiday_market()
            if holiday == 0:
                telegram_msg(string)
            else:
                telegram_justme('tatilaaaat! main function')
                print('holiday for log Queuev4.....')



            static = Market_with_askbid()
            static['symbol'] = static.index
            static = pd.merge(static,AVEGRAGE,how='left', on='id')
            static=static.rename(columns={'symbol_x' : 'symbol'})
            static.index = static['symbol']
            static['bidask'] = 0 
            static['bidask'][static['ask_price'] == static['max_allowed_price']] = 1
            static['bidask'][static['bid_price'] == static['min_allowed_price']] = -1
            
            static['heavyQ_notified'] = 0
            static['BIGCAP'] = 0
            pishgoshayesh=[]
            static['buy_queue_value'] = 0
            static['sell_queue_value'] = 0
            static['valueBQ_till_now'] = 0
            static['valueSQ_till_now'] = 0
            static['Buy_raftobargasht'] = 0
            static['Sell_raftobargasht'] = 0
            static['traded_value_in_queue'] = 0
            static['max_vol_in_buy_queue'] = static['Average_monthly_volume'].copy()
            static['max_vol_in_sell_queue']= static['Average_monthly_volume'].copy()


            
            percapita_buy_list =[]
            percapita_Wbuy_list =[]
            
            percapita_sell_list =[]
            percapita_Wsell_list =[]

            BuyQueue_values= []
            SellQueue_values = []
            BuyQueue_numbers = []
            SellQueue_numbers = []
            
            firsttime = (datetime.datetime.combine(datetime.datetime.now() , time_x.time()))
            start_time = (datetime.datetime.combine(datetime.datetime.now() , (time_x  +datetime.timedelta(minutes=0)).time()))  

            firsttime_trade_queue = (datetime.datetime.combine(datetime.datetime.now(),\
                                                               (time_x+datetime.timedelta(minutes=20)).time()))
            timelist = [firsttime]
            timelist_trade_queue = [firsttime_trade_queue]
            df_100 = pd.DataFrame(index=merged.columns[-20:-3] , columns=[i for i in range(100)])
            df_100[0] = 100
            m_100=0
            morning = 1


            for i in range(40):
                if i==0 :
                    firsttime += datetime.timedelta(minutes=10)
                else:
                    firsttime += datetime.timedelta(minutes=15)
                timelist.append(firsttime)
            for i in range(40):
                firsttime_trade_queue += datetime.timedelta(minutes=15)
                timelist_trade_queue.append(firsttime_trade_queue)
            print('morning goes to 1 and we can exit the first loop...')    
            for id in list(static['id'][static['Average_monthly_volume'].isna()]):
                try:
                    average_volz = average_month(id)
                    if average_volz > 1e6:
                        static['Average_monthly_volume'][static['id'] == id] = average_month(id)
                    else:
                        static['Average_monthly_volume'][static['id'] == id] = 1e6
                except:
                    static['Average_monthly_volume'][static['id'] == id] = 2e6
                    pass
                
            print('end of the Average monthly setting... eveything is ok for start!')
            telegram_justme('we are ready.... docker--- main function --- TSE---')
                
            
               
        elif time_x > (datetime.datetime.combine(time_x , datetime.time(12,35))):
            oo = (datetime.datetime.combine(datetime.datetime.now() , datetime.time(23,59)) -  \
                  datetime.datetime.now()).seconds + datetime.timedelta(hours=8, minutes=30).seconds

            holiday = 0
            morning=0
            second_check_static=0
            print('miram tu khabe zemestooonie yeruze')
            telegram_justme('miram tu khabe zemestooonie yeruze-- main function   ' )
            telegram_justme(str(each_iter))
            
            time.sleep(oo)
            
        elif time_x > (datetime.datetime.combine(time_x , datetime.time(8,50))) and    holiday ==0 and\
        time_x < (datetime.datetime.combine(time_x, datetime.time(12,31))) and morning==1:
            try:
                all_data = Market_with_askbid()
    
                instance_time = datetime.datetime.now()
                all_data['symbol'] =all_data.index
                all_data = all_data.sort_index()
                static = static.sort_index()
                all_data['traded_value_in_queue'] = 0


                all_data = pd.merge(all_data, AVEGRAGE[['id','Average_monthly_volume']],how='left')
                all_data.index = all_data['symbol']
                all_data['bidask'] = 0
                all_data['bidask'][all_data['ask_price'] == all_data['max_allowed_price']] = 1
                all_data['bidask'][all_data['bid_price'] == all_data['min_allowed_price']] = -1
#                 all_data['bidask'][(all_data['bid_price'] > all_data['min_allowed_price']) &\
#                                   (all_data['ask_price'] < all_data['max_allowed_price'])]= 0
                

                all_data['buy_queue_value'] = all_data['ask_vol'] * all_data['ask_price'] * all_data['bidask']
                all_data['buy_queue_value'][all_data['buy_queue_value']<0] = 0
                all_data['sell_queue_value']  = all_data['bid_vol'] * all_data['bid_price'] * all_data['bidask']
                all_data['sell_queue_value'][all_data['sell_queue_value']>0] = 0

                
                new_shares = [share for share in list(all_data.symbol) if share not in list(static.symbol)]

                for symbol in new_shares:
                    static = static.append(all_data[all_data['symbol'] == symbol])
                    static['BIGCAP'][ static['symbol'] ==symbol] = 0
                    static['bidask'][ static['symbol'] ==symbol] = 0
                    static['buy_queue_value'][ static['symbol'] ==symbol] = 0
                    static['sell_queue_value'][ static['symbol'] ==symbol] = 0
                    static['heavyQ_notified'][ static['symbol'] ==symbol] = 0
                    static['BIGCAP'][ static['symbol'] ==symbol] = 0
                    static['bidask'][ static['symbol'] ==symbol] = 0
                    static['buy_queue_value'][ static['symbol'] ==symbol] = 0
                    static['sell_queue_value'][ static['symbol'] ==symbol] = 0
                    static['valueBQ_till_now'][ static['symbol'] ==symbol] = 0
                    static['valueSQ_till_now'][ static['symbol'] ==symbol] = 0
                    static['Buy_raftobargasht'][ static['symbol'] ==symbol] = 0
                    static['Sell_raftobargasht'][ static['symbol'] ==symbol] = 0
                    if (all_data['Market_cap'][all_data['symbol'] == symbol].iloc[0] > 3e13) & (symbol not in pishgoshayesh):
                        test =' گشایش نماد💢 : ' + '#' +  symbol +URL(all_data['id'][all_data['symbol'] == symbol].iloc[0])
                        pishgoshayesh.append(symbol)
                        telegram_msg(test)
                    try:
                        av = AVEGRAGE['Average_monthly_volume'][AVEGRAGE['symbol'] ==symbol].iloc[0]
                        static['Average_monthly_volume'][static['symbol'] == symbol] = av
                        static['max_vol_in_buy_queue'][static['symbol'] == symbol] = av
                        static['max_vol_in_sell_queue'][static['symbol'] == symbol] = av
                        
                    except: 
                            static['Average_monthly_volume'][static['symbol'] == symbol] = 3e6
                            static['max_vol_in_buy_queue'][static['symbol'] == symbol] = 3e6
                            static['max_vol_in_sell_queue'][static['symbol'] == symbol] = 3e6
                            
                            pass
                new_shares = [share for share in list (all_data.symbol) if share not in list(merged.symbol)]     
                ISINz= [isin for isin in all_data['ISIN'][all_data['id'].isin(new_shares)] if not isin.startswith('IRR')]
                merged = pd.concat([merged, historical[historical['ISIN'].isin(ISINz)]])
                merged = merged.iloc[:,:-1]
                        
                new_shares2 = [share for share in list (static.symbol) if share not in list(all_data.symbol)]
                for symbol in new_shares2:
                    static = static.drop(static[static['symbol'] == symbol].index)
                all_data = all_data.sort_index()
                static = static.sort_index()

                
                all_data['av_vol30'] = static['Average_monthly_volume'].copy()
                all_data['Average_monthly_volume'] = static['Average_monthly_volume'].copy()
                all_data['adjust_queue_to_ret'] = np.nan
                all_data['Average_monthly_volume']= all_data['Average_monthly_volume'].apply(lambda x: np.nan if x< 10e3 else x )
                all_data['av_vol30']= all_data['av_vol30'].apply(lambda x: np.nan if x< 10e3 else x )
                
                all_data['adjust_queue_to_ret'][all_data['bidask'] == 1] =all_data['bidask'] *\
                ( all_data['ask_vol'][all_data['bidask'] == 1]/all_data['Average_monthly_volume'][all_data['bidask'] == 1]).\
                map(lambda x: 0.03* np.tanh(1.3*x))
                all_data['adjust_queue_to_ret'][all_data['bidask'] == -1] =all_data['bidask'] * \
                (all_data['bid_vol'][all_data['bidask'] == -1]/all_data['Average_monthly_volume'][all_data['bidask'] == -1]).\
                map(lambda x: 0.03 * np.tanh(1.3*x))
                all_data['adjust_queue_to_ret']=all_data['adjust_queue_to_ret'].fillna(0)
                buy_queue_df = all_data[ all_data['bidask'] ==1].sort_values('buy_queue_value', ascending = False)
                sell_queue_df = all_data[ all_data['bidask'] ==-1].sort_values('sell_queue_value', ascending = True)
                value_of_all_buyq = all_data['buy_queue_value'][ all_data['bidask'] ==1].sum()
                value_of_all_sellq = all_data['sell_queue_value'][ all_data['bidask'] ==-1].sum()

                if instance_time > timelist[0] :
                    m=15
                    string_for_buyq = ' ✅سنگین ترین صف خرید در نمادهای: \n\n' 
                    string_for_sellq =  ' \n 🛑 سنگین ترین صف فروش در نمادهای : \n\n'            
                    top5_buyq_symbols = list(buy_queue_df.head(m).index)
                    top5_sellq_symbols = list(sell_queue_df.head(m).index)

                    top5_buyq_values = list(buy_queue_df['buy_queue_value'].head(m))
                    top5_sellq_values = list(sell_queue_df['sell_queue_value'].head(m))
                    for item in list(buy_queue_df.head(m).symbol):
                        string_for_buyq += '#' +item +URL(all_data['id'][all_data['symbol'] == item].iloc[0]) +\
                        ' : ' + readable(all_data['buy_queue_value'][all_data['symbol'] == item].iloc[0]) + '\n'

                    for item in list(sell_queue_df.head(m).symbol):
                        string_for_sellq += '#' +item +URL(all_data['id'][all_data['symbol'] == item].iloc[0]) +\
                        ' : ' + readable(abs(all_data['sell_queue_value'][all_data['symbol'] == item].iloc[0])) + '\n'
                    string_for_queue_notifier = string_for_buyq + '\r'+string_for_sellq + '\n' +\
                    'ارزش کل صف های خرید : ' + '\n' +readable(buy_queue_df['buy_queue_value'].sum()) 
                    if len(BuyQueue_values)>=1 and float(buy_queue_df['buy_queue_value'].sum()) > float(BuyQueue_values[-1]):
                        string_for_queue_notifier += ' ( ' +\
                        convert_en_numbers(str(round(100 * (buy_queue_df['buy_queue_value'].sum() - BuyQueue_values[-1])\
                                  /BuyQueue_values[-1],2))) + '%+  افزایش✅ )'
                    elif len(BuyQueue_values)>=1  and buy_queue_df['buy_queue_value'].sum() < BuyQueue_values[-1]:
                        string_for_queue_notifier += ' ( ' +\
                        convert_en_numbers(str(abs(round(100* (buy_queue_df['buy_queue_value'].sum() - BuyQueue_values[-1])\
                                  /BuyQueue_values[-1],2)))) + '% کاهش🔻) '
                    else: pass

                    string_for_queue_notifier += '\n به تعداد: ' +convert_en_numbers(str(buy_queue_df.shape[0])) 
                    if len(BuyQueue_numbers)>=1 and buy_queue_df.shape[0] > BuyQueue_numbers[-1]:
                        string_for_queue_notifier += ' ( ' +\
                        convert_en_numbers(str(round( 100*(buy_queue_df.shape[0] - BuyQueue_numbers[-1])/\
                                  BuyQueue_numbers[-1] ,2) )) +'%+  افزایش✅ )'
                    elif  len(BuyQueue_numbers)>=1 and buy_queue_df.shape[0] < BuyQueue_numbers[-1]  :
                        string_for_queue_notifier += ' ( ' +\
                        convert_en_numbers(str(round(100*(buy_queue_df.shape[0]  - BuyQueue_numbers[-1])/\
                                  BuyQueue_numbers[-1],2))) +'% کاهش🔻) '      
                    else:pass

                    string_for_queue_notifier +='\n' +'ارزش کل صف های فروش: ' + '\n' +\
                    readable(abs(sell_queue_df['sell_queue_value'].sum()))
                    if len(SellQueue_values)>=1 and abs(sell_queue_df['sell_queue_value'].sum()) > SellQueue_values[-1]:
                        string_for_queue_notifier += ' ( ' +\
                        convert_en_numbers(str(round(100 * (abs(sell_queue_df['sell_queue_value'].sum()) - SellQueue_values[-1])\
                                  /SellQueue_values[-1],2))) + '%+   افزایش✅ )'
                    elif len(SellQueue_values)>=1  and abs(sell_queue_df['sell_queue_value'].sum()) < SellQueue_values[-1]:
                        string_for_queue_notifier += ' ( ' +\
                        convert_en_numbers(str(abs(round(100* (abs(sell_queue_df['sell_queue_value'].sum()) - SellQueue_values[-1])\
                                  /SellQueue_values[-1],2)))) + '%  کاهش🔻) '
                    else: pass



                    string_for_queue_notifier +='\n به تعداد  : '+ convert_en_numbers(str(sell_queue_df.shape[0]))
                    if len(SellQueue_numbers)>=1 and sell_queue_df.shape[0] > SellQueue_numbers[-1]:
                        string_for_queue_notifier += ' ( ' +\
                        convert_en_numbers(str(round(100 * (sell_queue_df.shape[0] - SellQueue_numbers[-1])\
                                  /SellQueue_numbers[-1],2))) + '%+   افزایش✅ )'
                    elif len(SellQueue_numbers)>=1  and sell_queue_df.shape[0] < SellQueue_numbers[-1]:
                        string_for_queue_notifier += ' ( ' +\
                        convert_en_numbers(str(abs(round(100* (sell_queue_df.shape[0] - SellQueue_numbers[-1])\
                                  /SellQueue_numbers[-1],2)))) + '%  کاهش🔻) '
                    else: pass

                    final_msgx = convert_en_numbers(jdatetime.datetime.now().strftime("%A, %d %b %Y ساعت - %M: %H"))\
                    +' 🇮🇷 ' +  "\n\n" +string_for_queue_notifier
                    msg =  ' #صف_خرید_فروش  \n ' +final_msgx
                    if instance_time < (datetime.datetime.combine(time_x, datetime.time(9,0))):
                        allz = Market_with_askbidzz()
                        all_ordervalue_in_sell = readable((allz['bid1_val'] + allz['bid2_val'] + allz['bid3_val']).sum())
                        all_ordervalue_in_buy =readable((allz['ask1_val'] + allz['ask2_val'] + allz['ask3_val']).sum())
                        msg = '🏁🏁 #پیش_گشایش 🏁🏁' + '\n\n' + 'مجموع کل سفارش های خرید : ' + all_ordervalue_in_buy +'\n'\
                        + ' مجموع کل سفارش های فروش: ' + all_ordervalue_in_sell + '\n' + final_msgx
                    telegram_msg((msg))
                    BuyQueue_values.append(buy_queue_df['buy_queue_value'].sum())
                    BuyQueue_numbers.append(buy_queue_df.shape[0])
                    SellQueue_values.append(abs(sell_queue_df['sell_queue_value'].sum()))
                    SellQueue_numbers.append(sell_queue_df.shape[0])
                    timelist.pop(0)
                    if time_x > (datetime.datetime.combine(time_x , datetime.time(9,13))):
                        clients = indiv()
                        clients['%Buy_I'] = clients['Individual_buy_volume'] / (clients['Individual_buy_volume'] +\
                                                                                clients['NonIndividual_buy_volume'])
                        clients['%Sell_N'] = clients['NonIndividual_sell_volume'] / (clients['NonIndividual_sell_volume'] +\
                                                                                    clients['Individual_sell_volume'])
                        clients['%%CodetoCode'] = clients['%Buy_I'] * clients['%Sell_N']
                        top = clients.sort_values('VAL_net_haghigh' , ascending = False)
                        bottom = clients.sort_values('VAL_net_haghigh' , ascending = True)
                        string = '#جدال_حقیقی_حقوقی💠 ⚔️💠 \n\n' + convert_en_numbers(jdatetime.datetime.now().strftime("%A, %d %b %Y ساعت - %M: %H"))\
                        +' 🇮🇷 '+ "\n"
                        if top['VAL_net_haghigh'].sum() > 0:
                            string +=  'ورود پول حقیقی : \n' + '✅💲' + readable(clients['VAL_net_haghigh'].sum()) + '\n'
                        else: string += 'خروج پول حقیقی :  ❌' + readable(abs(clients['VAL_net_haghigh'].sum())) + '\n\n'
                        I_buy_sarane_mean = clients['VAL_haghighi_BUY'].sum() / clients['Individual_buy_count'].sum()
                        string +='💵 ارزش معاملات تا این لحظه: '+'\n' +readable(clients.Value.astype(float).sum()) + '\n\n' 


                        I_sell_sarane_mean = clients['VAL_haghighi_SELL'].sum() / clients['Individual_sell_count'].sum()

                        I_weighted_buy_sarane_mean = (clients['percapita_buy'] * (clients['Value'] / clients['Value'].sum())).sum()
                        string += ' \n'+'🔍 میانگین وزندار سرانه های خرید حقیقی : ' + '\n' +\
                        readable(I_weighted_buy_sarane_mean) 
                        
                        if len(percapita_Wbuy_list)>0 and \
                        percapita_Wbuy_list[-1] < I_weighted_buy_sarane_mean:
                            string += ' ( ' +convert_en_numbers(str(round(100*(-percapita_Wbuy_list[-1] +I_weighted_buy_sarane_mean )/\
                                                       percapita_Wbuy_list[-1],1) ))+'%+  افزایش✅ ) \n'
                        elif len(percapita_Wbuy_list)>0 and \
                        percapita_Wbuy_list[-1] > I_weighted_buy_sarane_mean:
                            string += ' ( ' + convert_en_numbers(str(round(100*(percapita_Wbuy_list[-1] -I_weighted_buy_sarane_mean)/\
                                                       percapita_Wbuy_list[-1],1) ))+'%  کاهش🔻 ) \n'
                        else:
                            string += '\n'
                            pass
                        I_weighted_sell_sarane_mean = (clients['percapita_sell'] * (clients['Value'] / clients['Value'].sum())).sum()
                        string += 'میانگین وزندار سرانه های فروش حقیقی : ' +'\n' +readable(I_weighted_sell_sarane_mean)

                        if len(percapita_Wsell_list)>0 and \
                        percapita_Wsell_list[-1] < I_weighted_sell_sarane_mean:
                            string += ' ( ' +convert_en_numbers(str(round(100*(-percapita_Wsell_list[-1] +I_weighted_sell_sarane_mean )/\
                                                       percapita_Wsell_list[-1],1) ))+'%+  افزایش✅ ) \n'
                        elif len(percapita_Wsell_list)>0 and \
                        percapita_Wsell_list[-1] > I_weighted_sell_sarane_mean:
                            string += ' ( ' + convert_en_numbers(str(round(100*(percapita_Wsell_list[-1] - I_weighted_sell_sarane_mean )/\
                                                       percapita_Wsell_list[-1],1) ))+'%   کاهش🔻 ) \n'
                        else:
                            string += '\n'
                            pass
                        percapita_buy_list.append(I_buy_sarane_mean)
                        percapita_sell_list.append(I_sell_sarane_mean)
                        percapita_Wbuy_list.append(I_weighted_buy_sarane_mean)
                        percapita_Wsell_list.append(I_weighted_sell_sarane_mean)

                        string+='\n\n'+' 🌊💰بیشترین ورود نقدینگی حقیقی در نماد های :\n\n ' 
                        for item in list(top.head(13).Name):
                            string+=  '#' + item +' : ' +readable(top['VAL_net_haghigh'][top['Name'] == item].iloc[0])+\
                            URL(clients['id'][clients['Name'] == item].iloc[0]) + '\n'
                        string += ' \n💸⛔بیشترین خروج نقدینگی حقیقی در نماد های :\n\n'
                        for item in  list(bottom.head(13).Name):
                            string +=  '#' + item + ' : ' +readable(abs(bottom['VAL_net_haghigh'][bottom['Name'] == item].iloc[0]))+\
                            URL(bottom['id'][bottom['Name'] == item].iloc[0]) + '\n'
                        string += ' \n🥇☢️بیشترین قدرت حقیقی در نمادهای : \n\n '

                        top_power = clients.sort_values('power' , ascending=False)
                        top_power = top_power[(top_power['VAL_haghighi_BUY'] > 2e10) &(top_power['Individual_buy_count'] > 100)&\
                                              (top_power['percapita_sell'] > 3e7) & ( top_power['percapita_buy']> 5e7)]
                        top_power.dropna(inplace=True)
                        for item in list(top_power.head(13).Name):
                            string += '#' + item + ' : ' + ' با قدرت خرید ' + convert_en_numbers(str(round(top_power['power'][top_power['Name'] == item].iloc[0]))) +\
                            URL(clients['id'][clients['Name'] == item].iloc[0]) + '\n'

                        telegram_msg((string))

                all_data = all_data.sort_index()
                static = static.sort_index()
                
                static['traded_value_in_queue'] = static['bidask'] * ( all_data['value'] - static['value'])
                static['traded_value_in_queue'][static['bidask'] != 1 ] = 0
                static['valueBQ_till_now'] += static['traded_value_in_queue']
                static['traded_value_in_queue'] = 0 
                static['traded_value_in_queue'] = static['bidask'] * ( all_data['value'] - static['value'])
                static['traded_value_in_queue'][static['bidask'] != -1 ] = 0       
                static['valueSQ_till_now'] -= static['traded_value_in_queue']
                static['traded_value_in_queue'] = 0 
                

                if instance_time >  timelist_trade_queue[0] :
                    m=10
                    clienttype = indiv()
                    string_col=  'تغییرات شاخص صنایع مختلف از ابتدای بازار تا به این لحظه📊📉: \n\n'
                    for i, col in enumerate(merged.columns[-20:-3]):
                        collist=  list(merged['id'][merged[col] == 1])
                        col_index = all_data['id'].isin(collist)
                        ret_each_industry = round((100*(all_data['last_ret'][col_index] * all_data['Market_cap'][col_index] )/\
                        (all_data['Market_cap'][col_index].sum() )).sum(),2)
                        selected_industry = clienttype['VAL_net_haghigh'][clienttype['id'].isin(collist)].nlargest(1)
                        which_share = clienttype['Name'].loc[selected_industry.index[0]]
                        amount_of_money = selected_industry.iloc[0]
                        selected_industry_power = clienttype['power'][clienttype['id'].isin(collist)].nlargest(1)
                        which_share_power = clienttype['Name'].loc[selected_industry_power.index[0]]
                        which_power = selected_industry_power.iloc[0]
                        moneyflow_to_index = readable(clienttype['VAL_net_haghigh'][clienttype['id'].isin(collist)].sum())
                        if ret_each_industry >= 0 :
                            string_col += ' ❇️ ' +  '<strong>{}'.format(col)  + ' ' +convert_en_numbers(str(ret_each_industry)) \
                            +'% </strong>'+ ' ➖ ' + moneyflow_to_index + '\n' +  'بیشترین ورود پول به : ' + which_share +' : ' +\
                                readable(amount_of_money) + '\n بیشترین قدرت خرید حقیقی در: ' + which_share_power +  '\n' +\
                                convert_en_numbers(str(all_data[col_index][all_data['bidask'] == 1].shape[0])) + 'سهم در صف خرید  به ارزش : ' + \
                            readable(all_data['buy_queue_value'][col_index][all_data['bidask'] == 1].sum()) +'\n'+ \
                             convert_en_numbers(str(all_data[col_index][all_data['bidask'] == -1].shape[0])) + ' سهم در صف فروش  به ارزش : ' + \
                            readable(-all_data['sell_queue_value'][col_index][all_data['bidask'] == -1].sum())+'\n'
                        else:
                             string_col += ' ⭕ ' +  '<strong>{}'.format(col) + ' ' +convert_en_numbers(str(ret_each_industry))\
                                +'% </strong>'+ ' ➖ ' +  moneyflow_to_index + '\n' + 'بیشترین ورود پول به : ' + which_share+ ' : ' +\
                                readable(amount_of_money) + '  \n بیشترین قدرت خرید حقیقی در: ' + which_share_power +'\n'+\
                                convert_en_numbers(str(all_data[col_index][all_data['bidask'] == 1].shape[0]) )+ ' سهم در صف خرید  به ارزش :  ' + \
                            readable(all_data['buy_queue_value'][col_index][all_data['bidask'] == 1].sum()) +'\n'+\
                                 convert_en_numbers(str(all_data[col_index][all_data['bidask'] == -1].shape[0])) + ' سهم در صف فروش  به ارزش : ' + \
                            readable(-all_data['sell_queue_value'][col_index][all_data['bidask'] == -1].sum()) +'\n'
                    if instance_time > (datetime.datetime.combine(time_x, datetime.time(9,6))):
                        telegram_msg((string_col))
                        timelist_trade_queue.pop(0)
                    
                for i, col in enumerate(merged.columns[-20:-3]):
                    collist=  list(merged['id'][merged[col] == 1])
                    selected_index = all_data['id'].isin(collist)
                    all_data['last_ret_adj'] = all_data['last_ret'] + all_data['adjust_queue_to_ret']
                    weighted_ret_index  = ((all_data['last_ret_adj'][selected_index] * all_data['Market_cap'][selected_index] )/\
                    (all_data['Market_cap'][selected_index].sum())).sum()
                    df_100.iloc[i,((m_100+1)%100)] = (1 +  weighted_ret_index ) * 100

#                 df_100 = df_100.rename(columns={df_100.columns[(m_100+1)%100]: datetime.datetime.now().replace(microsecond=0)})

                
                try:
                    for i in range(df_100.shape[0]):
                        mm = (m_100+1)%100
                        if (df_100.iloc[i, mm] > 1.004 * df_100.iloc[i].mean()) and\
                        (df_100.iloc[i].max() - df_100.iloc[i].min()) > 1.5:
                            string = ' تغییرات مثبت در شاخص 🧿 ' + '<strong>{}</strong>'.format(df_100.index[i])
                            df_100.iloc[i] = np.nan
                            if instance_time >   datetime.datetime.combine(datetime.datetime.now(), datetime.time(9,6)):
                                telegram_msg((string))
                                
                        if (df_100.iloc[i, mm] < 0.996 * df_100.iloc[i].mean()) and\
                        (df_100.iloc[i].max() - df_100.iloc[i].min() > 1.5):
                            string = '🩸🔻 تغییرات منفی در شاخص ' + '<strong>{}</strong>'.format(df_100.index[i])
                            df_100.iloc[i] = np.nan
                            if instance_time >   datetime.datetime.combine(datetime.datetime.now(), datetime.time(9,6)) :
                                telegram_msg((string))
                                
                            
                        
                except:
                    pass
                m_100+=1
                            
                        
                    
                    
                
                
                
                static['Buy_raftobargasht'][(all_data['bidask'] == 0) & (static['bidask'] == 1) & (static['Market_cap'] > 3e13)] += 1
                static['Sell_raftobargasht'][(all_data['bidask'] == 0) & (static['bidask'] == -1) & (static['Market_cap'] > 3e13)] += 1
                
                repeated_buy_queue = static[static['Buy_raftobargasht'] > np.random.randint(15,100)]
                repeated_sell_queue = static[(static['Sell_raftobargasht'] >np.random.randint(15,100))]

                if repeated_buy_queue.empty == False:
                    repeated_buy_queue_str = ' 👾🔃💫عرضه ی مکرر صف خرید در نماد (های): \n\n' 
                    for item in list(repeated_buy_queue.symbol):
                        repeated_buy_queue_str +=  '#' + item  +  URL(all_data['id'][all_data['symbol'] == item].iloc[0]) +'\n'
                        static['Buy_raftobargasht'][static['symbol'] == item] = 0
                    try:
                        if instance_time > start_time :
                            telegram_msg((repeated_buy_queue_str))

                    except:pass
                if repeated_sell_queue.empty == False:
                    repeated_sell_queue_str = ' 🃏🔄🎃جمع شدن مکرر صف فروش در نماد(های): \n\n' 
                    for item in list(repeated_sell_queue.symbol):
                        repeated_sell_queue_str +=  '#' + item  +  URL(all_data['id'][all_data['symbol'] == item].iloc[0]) +'\n'
                        static['Sell_raftobargasht'][static['symbol'] == item] = 0
                    try:
                        if instance_time > start_time :
                            telegram_msg((repeated_sell_queue_str))

                    except:
                        pass


                
                heavy_buy_queue =  all_data[(all_data['bidask'] == 1) & (all_data['ask_vol'] > static['max_vol_in_buy_queue']) &\
                                           (static['heavyQ_notified'] == 0) & (all_data['Market_cap'] > 3e13)&(all_data['buy_queue_value'] > 4e10)]

                heavy_sell_queue =  all_data[(all_data['bidask'] == -1) & (all_data['bid_vol'] > static['max_vol_in_sell_queue']) &\
                                             (static['heavyQ_notified'] == 0) & (all_data['Market_cap'] > 3e13) &(abs(all_data['sell_queue_value']) > 4e10)] 
                
                bigcap_buy_queue = all_data[(all_data['bidask'] == 1) & (all_data['Market_cap'] > 7e13) & (static['BIGCAP'] < 1)]
                
                bigcap_sell_queue = all_data[(all_data['bidask'] == -1) & (all_data['Market_cap'] > 7e13) & (static['BIGCAP'] > -1)]
                
                static['max_vol_in_buy_queue'] = pd.concat([static['max_vol_in_buy_queue'],all_data['ask_vol']] , axis=1).max(axis=1)
                static['max_vol_in_sell_queue'] = pd.concat([static['max_vol_in_sell_queue'],all_data['bid_vol']] , axis=1).max(axis=1)
                
                if bigcap_buy_queue.empty == False: 
                    bigcap_buy_queue_str = 'تشکیل صف خرید  در نماد(های) 🏦✅🌟 : ' + '\n\n'
                    if instance_time < (datetime.datetime.combine(time_x, datetime.time(9,0))):
                        bigcap_buy_queue_str = '🏁🏁 صفوف خرید مهم  در پیش گشایش 🏦✅ : ' + '\n\n'
                        
                    for item in list(bigcap_buy_queue.symbol):
                        bigcap_buy_queue_str +=  '#' + item + URL(all_data['id'][all_data['symbol'] == item].iloc[0]) \
                        +'  به تعداد  : ' +readable_tedad(all_data['ask_vol'][all_data['symbol'] == item ].iloc[0])+ ' به ارزش: '+\
                        readable(all_data['buy_queue_value'][all_data['symbol'] == item ].iloc[0] ) + '\n'
                        static['BIGCAP'][static['symbol'] == item] = 1
                    if instance_time > start_time :
                        telegram_msg((bigcap_buy_queue_str))
                                    


                if bigcap_sell_queue.empty == False:
                    bigcap_sell_queue_str = '🛑💀🛒تشکیل صف فروش  در نماد(های)  : ' + '\n\n'
                    if instance_time < (datetime.datetime.combine(time_x, datetime.time(9,0))):
                        bigcap_buy_queue_str = '🏁🏁 صفوف فروش مهم  در پیش گشایش 🍂🛒 : ' + '\n\n'
                    for item in list(bigcap_sell_queue.symbol):
                        bigcap_sell_queue_str +=  '#' + item + URL(all_data['id'][all_data['symbol'] == item].iloc[0]) \
                        +'به تعداد : ' +readable_tedad(all_data['bid_vol'][all_data['symbol'] == item ].iloc[0])+ ' به ارزش: '+\
                        readable(abs(all_data['sell_queue_value'][all_data['symbol'] == item ].iloc[0]) ) + '\n'
                        static['BIGCAP'][static['symbol'] == item] = -1

                    if instance_time > start_time :
                        telegram_msg((bigcap_sell_queue_str))

                if heavy_buy_queue.empty == False:
                    Heavy_buy_queue_str = '🏋️📈🔥سنگین شدن صف خرید در نماد(های): '  + '\n'
                    if instance_time < (datetime.datetime.combine(time_x, datetime.time(9,0))):
                        Heavy_buy_queue_str =  '🏁🏁 صفوف سنگین خرید در  پیش گشایش  📈🏋️🔥: ' + '\n'
                    for item in list(heavy_buy_queue.symbol):
                        Heavy_buy_queue_str += '#' + item + URL(all_data['id'][all_data['symbol'] == item].iloc[0]) \
                        +'به تعداد : ' +readable_tedad(all_data['ask_vol'][all_data['symbol'] == item ].iloc[0])+ ' به ارزش: '+\
                        readable(all_data['buy_queue_value'][all_data['symbol'] == item ].iloc[0] ) + '\n'
                        static['heavyQ_notified'][static['symbol'] == item] = 1
                    if instance_time > start_time :
                        telegram_msg(Heavy_buy_queue_str)

                if heavy_sell_queue.empty == False:
                    Heavy_sell_queue_str = 'سنگین شدن صف فروش در نماد(های)🩸 ☠️: '  + '\n'
                    if instance_time < (datetime.datetime.combine(time_x, datetime.time(9,0))):
                        Heavy_sell_queue_str =  '🏁🏁 صفوف سنگین فروش در پیش گشایش  🩸🏮: ' + '\n'
                    for item in list(heavy_sell_queue.symbol):
                        Heavy_sell_queue_str += '#' + item + URL(all_data['id'][all_data['symbol'] == item].iloc[0]) \
                        +'به تعداد : ' +readable_tedad(all_data['bid_vol'][all_data['symbol'] == item ].iloc[0])+ ' به ارزش: '+\
                        readable(abs(all_data['sell_queue_value'][all_data['symbol'] == item ].iloc[0])) + '\n'
                        static['heavyQ_notified'][static['symbol'] == item] = -1
                    if instance_time > start_time :
                        telegram_msg((Heavy_sell_queue_str))

                heavy_buy_queue_dissappear = all_data[(all_data['bidask'] == 1) & \
                                                      (all_data['ask_vol'] < 0.5 *static['max_vol_in_buy_queue']) &\
                                                      (static['heavyQ_notified'] == 1)]
                heavy_sell_queue_dissappear = all_data[(all_data['bidask'] == -1) &\
                                                       (all_data['bid_vol'] < 0.5 *static['max_vol_in_sell_queue']) &\
                                                       (static['heavyQ_notified'] == -1)]

                if heavy_buy_queue_dissappear.empty == False:
                    heavy_buy_queue_dissappear_str = 'سبک شدن صف های سنگین خرید در نماد (های):💨🍃 ' + '\n\n'
                    for item in list(heavy_buy_queue_dissappear.symbol):
                        heavy_buy_queue_dissappear_str += '#' + item + URL(all_data['id'][all_data['symbol'] == item].iloc[0]) + '\n'
                        static['heavyQ_notified'][static['symbol'] == item] = 0
                    if instance_time > start_time :
                        telegram_msg((heavy_buy_queue_dissappear_str))

                if heavy_sell_queue_dissappear.empty == False:
                    heavy_sell_queue_dissappear_str = 'سبک شدن صف های سنگین  فروش در نماد (های)💨🔦 ' + '\n\n'
                    for item in list(heavy_sell_queue_dissappear.symbol):
                        heavy_sell_queue_dissappear_str += '#' + item + URL(all_data['id'][all_data['symbol'] == item].iloc[0]) + '\n'
                        static['heavyQ_notified'][static['symbol'] == item] = 0
                    if instance_time > start_time :
                        telegram_msg((heavy_sell_queue_dissappear_str))

                static['BIGCAP'][(all_data['bidask'] == 1) & (all_data['Market_cap'] > 6e13) \
                                 & (all_data['ask_vol'] > 0.1* all_data['av_vol30'])&(all_data['buy_queue_value'] > 2e10)] = 3
                
                bigcap_buy_queue_dissappeared = all_data[(static['BIGCAP'] == 3)  &(all_data['ask_price'] < all_data['max_allowed_price'])  ]
                
                if bigcap_buy_queue_dissappeared.empty == False:
                    bigcap_buy_queue_dissappeared_str = ' : 🚨💥‼️ :عرضه ی کامل صف خرید در نماد (های)' + '\n' 
                    if instance_time < (datetime.datetime.combine(time_x, datetime.time(9,0))):
                        bigcap_buy_queue_dissappeared_str =  '🏁🏁 کاهش بهترین قیمت تقاضا از بیشترین قیمت مجاز روز در  پیش گشایش  📢 🏮: ' + '\n'
                    for item in list(bigcap_buy_queue_dissappeared.symbol):
                        bigcap_buy_queue_dissappeared_str += '#' + item + URL(all_data['id'][all_data['symbol'] == item].iloc[0]) + '\n'
                        static['BIGCAP'][static['symbol'] == item] = 0
                    if instance_time > start_time :
                        telegram_msg((bigcap_buy_queue_dissappeared_str))


                static['BIGCAP'][(all_data['bidask'] == -1) & (all_data['Market_cap'] > 6e13) \
                                 & (all_data['bid_vol'] > 0.1* all_data['av_vol30'])&(abs(all_data['sell_queue_value']) > 2e10)] = -3
                
                bigcap_sell_queue_dissappeared = all_data[(static['BIGCAP'] == -3) &((all_data['bid_price'] > all_data['min_allowed_price']))] 
                
                if bigcap_sell_queue_dissappeared.empty == False:
                    bigcap_sell_queue_dissappeared_str = ' : 💥😍🔓:جمع شدن کامل صف فروش در نماد (های)' + '\n\n' 
                    if instance_time < (datetime.datetime.combine(time_x, datetime.time(9,0))):
                        bigcap_sell_queue_dissappeared_str =  '🏁🏁 افزایش بهترین قیمت عرضه از کمترین قیمت مجاز روز در  پیش گشایش  💥👾: ' + '\n'
                    for item in list(bigcap_sell_queue_dissappeared.symbol):
                        bigcap_sell_queue_dissappeared_str += '#' + item + URL(all_data['id'][all_data['symbol'] == item].iloc[0]) + '\n'
                        static['BIGCAP'][static['symbol'] == item] = 0
                    if instance_time > start_time :
                        telegram_msg((bigcap_sell_queue_dissappeared_str))
                        
                static['bidask'] = all_data['bidask'].copy()
                static['value'] = all_data['value'].copy()
                each_iter += 1

            except:
                time.sleep(3)

                pass
        else:
            time.sleep(5)
    except:
        time.sleep(5)






