from persiantools.jdatetime import JalaliDate
import statistics
import time
import re
import jdatetime
import datetime

import os
import json
import requests
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings('ignore')
jdatetime.set_locale('fa_IR')


def market():
    url='http://www.tsetmc.com/tsev2/data/MarketWatchPlus.aspx'
    data = requests.get(url,timeout=15)
    content = data.content.decode('utf-8')

    parts = content.split('@')
    inst_price = parts[2].split(';')
    market = { }
    for item in inst_price:
        item=item.split(',')
        market[item[2]]= dict(id=item[0],ISIN=item[1],symbol=convert_ar_characters(item[2]),
                              name=item[3],first_price=item[5],close_price=item[6],
                              last_price=item[7],count=item[8],volume=item[9],
                              value=item[10],min_traded_price=item[11],
                              max_treaded_price=item[12],yesterday_price=item[13],
                              eps=item[14],base_volume=item[15],c2=item[16],
                              table_id=item[17],group_id=item[18],max_allowed_price=item[19],
                              min_allowed_price=item[20],type_of_symbol=item[22],
                              number_of_shares=item[21], Market_cap=int(item[21]) *int(item[6]) )
    return market
def indiv():
    count = 0
    while count <= 10:
        url = 'http://www.tsetmc.com/tsev2/data/ClientTypeAll.aspx'
        data = requests.get(url, timeout=10)
        content=data.content.decode('utf-8').split(";")
        if data.status_code != 200:
            count += 1
            time.sleep(0.5)
        elif data.status_code == 200:
            break
        elif count == 10:
            raise Exception('Loop Client!')
        else:
            time.sleep(1)
            pass
    all_data = pd.DataFrame(market()).T
    all_data = all_data[all_data['ISIN'].map(lambda x:not ( x.startswith('IRT')))]
    all_data = all_data[all_data['ISIN'].map(lambda x:not ( x.startswith('IRB')))]
    
    others = ['آ س پ' , 'جم پيلن', 'كي بي سي' , 'فن آوا' , 'انرژي3' , 'دتهران\u200c', 'هاي وب']
    for item in list(all_data.index):
        if item.isalpha() ==False:
            if item in others:
                continue
            all_data = all_data.drop(item, axis = 0)

    if True:
        all_data['num'] = [i for i in range(all_data.shape[0])]
        all_data = all_data.set_index('num')
    clienttype=[]
    for item in content:
        try:
            item=item.split(',')
            symbol = all_data[ all_data['id'] ==  (item[0]) ]['symbol'].iloc[0]
            clienttype.append(dict( id=item[0],Name = symbol,
                                       Individual_buy_count=int(item[1]),
                                          NonIndividual_buy_count=int(item[2]),
                                          Individual_buy_volume=int(item[3]),
                                          NonIndividual_buy_volume=int( item[4]) ,
                                          Individual_sell_count=int(item[5]),
                                          NonIndividual_sell_count=int(item[6]),
                                          Individual_sell_volume=int(item[7]),
                                          NonIndividual_sell_volume=int(item[8]),
                                         Value = float(all_data[all_data['symbol'] == symbol]['value'])))
        except:
            continue
    clients = pd.DataFrame(clienttype)
    clients.Name = clients.Name.map(lambda x: convert_ar_characters(x) )
    clients['VAL_hoghooghi_SELL'] = clients['Value'] * clients['NonIndividual_sell_volume'].astype(float) /\
    (clients['Individual_sell_volume'].astype(float) +clients['NonIndividual_sell_volume'].astype(float))

    clients['VAL_haghighi_BUY'] = clients['Value'] * clients['Individual_buy_volume'].astype(float) /\
    (clients['Individual_buy_volume'].astype(float) +clients['NonIndividual_buy_volume'].astype(float))
    clients['VAL_haghighi_SELL'] = clients['Value'] * clients['Individual_sell_volume'].astype(float) /\
    (clients['Individual_sell_volume'].astype(float) +clients['NonIndividual_sell_volume'].astype(float))
    clients['percapita_buy'] = clients['VAL_haghighi_BUY'] / clients['Individual_buy_count']
    clients['percapita_sell'] = clients['VAL_haghighi_SELL'] / clients['Individual_sell_count']
    clients['power'] = clients['percapita_buy'] / clients['percapita_sell']
    clients['VAL_net_haghigh'] = clients['VAL_haghighi_BUY'] - clients['VAL_haghighi_SELL']

    return clients 
def Market_with_askbid():
    count = 0 
    while count<15:
        url = 'http://www.tsetmc.com/tsev2/data/MarketWatchPlus.aspx?h=0&r=0'
        data = requests.get(url, timeout=12)
        content = data.content.decode('utf-8')
        parts = content.split('@')
        if data.status_code != 200 or len(content.split('@')[2])<400:
            count+=1
            time.sleep(1)
        if count ==15:
            raise Exception('ohoh')
        if data.status_code == 200 and len(content.split('@')[2]) > 400:
            break
    parts = content.split('@')
    inst_price = parts[2].split(';')
    market_me = {}
    # Add the Trade and other stuff to dataframe--------
    for item in inst_price:
        item=item.split(',')
        market_me[item[0]]= dict(id=item[0],ISIN=item[1],symbol=item[2],
                              name=item[3],first_price=float(item[5]),close_price=float(item[6]),
                              last_trade=float(item[7]),count=item[8],volume=float(item[9]),
                              value=float(item[10]),min_traded_price=float(item[11]),
                              max_treaded_price=float(item[12]),yesterday_price=int(item[13]),
                              table_id=item[17],group_id=item[18],max_allowed_price=float(item[19]),
                              min_allowed_price=float(item[20]),last_ret = (float(item[7]) - float(item[13]))/float(item[13]),
                                 ret = (float(item[6]) - float(item[13]))/float(item[13]),
                                number_of_shares=float(item[21]), Market_cap=int(item[21]) *int(item[6]))
    # Add the Ask-Bid price Vol tu dataframe --------
    for item in parts[3].split(';'):
        try:
            item=item.split(',')
            if item[1] == '1':
                market_me[item[0]]['ask_price'.format(item[1])]=  float(item[4])
                market_me[item[0]]['ask_vol'.format(item[1])]=  float(item[6])
                
                market_me[item[0]]['bid_price'.format(item[1])]=  float(item[5])
                market_me[item[0]]['bid_vol'.format(item[1])]=  float(item[7])

        except:
            pass
    df = pd.DataFrame(market_me).T
    df = df[df['ISIN'].map(lambda x:not ( x.startswith('IRT')))]
    df = df[df['ISIN'].map(lambda x: not( x.startswith('IRB')))]
    df = df[df['symbol'].map(lambda x: x.isalpha())]
    df.symbol = df.symbol.map(lambda x: convert_ar_characters(x) )
    df = df.set_index('symbol')
    return df
def readable(n):
    human_readable =''
    if float(n) < 0 : 
        human_readable += '-'
    n = abs(float(n))
    if n >= 1e7 and n<= 1e10 :
        round_number = n/1e7
        human_readable += '{:,.0f}{}'.format(round_number,   ' میلیون تومان ')
    elif n>1e10:
        round_number = n/1e10
        
        human_readable += '{:,.0f}{}'.format(round_number, ' میلیارد تومان ')
    else:
        round_number = n/10
        human_readable += '{:,.0f}{}'.format(round_number, 'تومان')
        
    return convert_en_numbers(human_readable)
def readable_tedad(n):
    human_readable =''
    if int(n) < 0 : 
        human_readable += '-'
    n = abs(int(n))
    if n >= 1e6 and n<= 1e9 :
        round_number = n/1e6
        human_readable += '{:,.1f}{}'.format(round_number,   ' میلیون ')
    elif n>1e9:
        round_number = n/1e9
        
        human_readable += '{:,.1f}{}'.format(round_number, ' میلیارد ')
    else:
        human_readable += '{:,}'.format(n)
    return convert_en_numbers(human_readable)
def average_month(id):
    counter = 0
    while counter < 10:
        url = 'http://members.tsetmc.com/tsev2/data/InstTradeHistory.aspx?i={}&Top=20&A=1'.format(
            id)
        data = requests.get(url, timeout=8, verify=False)
        content = data.content.decode('utf-8').split(';')
        if data.status_code == 200 and len(content) > 5:
            break
        else:
            counter += 1
    hist_vol = []
    for i in range(len(content) - 1):
        hist_vol.append(float(content[i].split('@')[-2]))
    mean = statistics.mean(hist_vol)
    return mean
def telegram_msg(msg, chat_id="-1001470501669"):
    headers = {'Content-type': 'application/json'}
    payload = {
        "bot-name": "hermes",
        "chat-id": chat_id,
        "message": msg,
        "parse-mode": "html"
    }
    r = requests.post('http://178.62.251.62:8891/send-message',
                      headers=headers,
                      data=json.dumps(payload))
    payload = {"bot-name" : "hermes" ,
                  "chat-id" : '-1001453076404',
                  "message" : msg,
                  "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message',
                          headers = headers,
                          data=json.dumps(payload))
    payload = {"bot-name" : "hermes" ,
              "chat-id" : '-1001279557836',
              "message" : msg,
              "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message',
                      headers = headers,
                      data=json.dumps(payload))
    payload = {"bot-name" : "hermes" ,
              "chat-id" : '-1001437707813',
              "message" : msg,
              "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message',
                      headers = headers,
                      data=json.dumps(payload))

    if r.status_code == 200:
        return True
    else:
        return False
def URL(id):
    if type(id) == str:
        URLp = 'http://www.tsetmc.com/Loader.aspx?ParTree=151311&i={}'.format(
            id)
    else:
        id = str(id)
        URLp = 'http://www.tsetmc.com/Loader.aspx?ParTree=151311&i={}'.format(
            id)
    return '<a href="{}"> -{}</a>'.format(URLp, '(TSETMC)')
def holiday_market():
    static = Market_with_askbid()
    time.sleep(5)
    static2 = Market_with_askbid()
    boolz = static['bid_vol'].sum() == static2['bid_vol'].sum()
    if boolz:
        holiday = 1
    else:
        holiday=0
    return holiday
def telegram_msg_just_me(msg , chat_id = "80911655"):
    headers = {'Content-type': 'application/json'}
    payload = {"bot-name" : "hermes" , 
              "chat-id" : chat_id,
              "message" : msg,
              "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                      headers = headers,
                      data=json.dumps(payload))
def convert_en_numbers(input_str):
    """
    Converts English numbers to Persian numbers
    :param input_str: String contains English numbers
    :return: New string with Persian numbers
    """
    mapping = {
        '0': '۰',
        '1': '۱',
        '2': '۲',
        '3': '۳',
        '4': '۴',
        '5': '۵',
        '6': '۶',
        '7': '۷',
        '8': '۸',
        '9': '۹',
        '.': '.',
    }
    return _multiple_replace(mapping, input_str)
def convert_en_characters(input_str):
    """
        Assumes that characters written with standard persian keyboard
        not windows arabic layout
    :param input_str: String contains English chars
    :return: New string with related characters on Persian standard keyboard
    """
    mapping = {
        'q': 'ض',
        'w': 'ص',
        'e': 'ث',
        'r': 'ق',
        't': 'ف',
        'y': 'غ',
        'u': 'ع',
        'i': 'ه',
        'o': 'خ',
        'p': 'ح',
        '[': 'ج',
        ']': 'چ',
        'a': 'ش',
        's': 'س',
        'd': 'ی',
        'f': 'ب',
        'g': 'ل',
        'h': 'ا',
        'j': 'ت',
        'k': 'ن',
        'l': 'م',
        ';': 'ک',
        "'": 'گ',
        'z': 'ظ',
        'x': 'ط',
        'c': 'ز',
        'v': 'ر',
        'b': 'ذ',
        'n': 'د',
        'm': 'پ',
        ',': 'و',
        '?': '؟',
    }
    return _multiple_replace(mapping, input_str)
def convert_ar_numbers(input_str):
    """
    Converts Arabic numbers to Persian numbers
    :param input_str: String contains Arabic numbers
    :return: New str and replaces arabic number with persian numbers
    """
    mapping = {
        '١': '۱',  # Arabic 1 is 0x661 and Persian one is 0x6f1
        '٢': '۲',  # More info https://goo.gl/SPiBtn
        '٣': '۳',
        '٤': '۴',
        '٥': '۵',
        '٦': '۶',
        '٧': '۷',
        '٨': '۸',
        '٩': '۹',
        '٠': '۰',
    }
    return _multiple_replace(mapping, input_str)
def convert_fa_numbers(input_str):
    """
    This function convert Persian numbers to English numbers.
    
    Keyword arguments:
    input_str -- It should be string
    Returns: English numbers
    """
    mapping = {
        '۰': '0',
        '۱': '1',
        '۲': '2',
        '۳': '3',
        '۴': '4',
        '۵': '5',
        '۶': '6',
        '۷': '7',
        '۸': '8',
        '۹': '9',
        '.': '.',
    }
    return _multiple_replace(mapping, input_str)
def convert_ar_characters(input_str):
    """
    Converts Arabic chars to related Persian unicode char
    :param input_str: String contains Arabic chars
    :return: New str with converted arabic chars
    """
    mapping = {
        'ك': 'ک',
        'دِ': 'د',
        'بِ': 'ب',
        'زِ': 'ز',
        'ذِ': 'ذ',
        'شِ': 'ش',
        'سِ': 'س',
        'ى': 'ی',
        'ي': 'ی'
    }
    return _multiple_replace(mapping, input_str)
def _multiple_replace(mapping, text):
    """
    Internal function for replace all mapping keys for a input string
    :param mapping: replacing mapping keys
    :param text: user input string
    :return: New string with converted mapping keys to values
    """
    pattern = "|".join(map(re.escape, mapping.keys()))
    return re.sub(pattern, lambda m: mapping[m.group()], str(text))
def main_index():
    from persiantools.jdatetime import JalaliDate
    import requests
    import pandas as pd
    url='http://www.tsetmc.com/tsev2/chart/data/Index.aspx?i=32097828799138957&t=value'
    data=requests.get(url)
    content= data.content.decode('utf-8').split(';')
    dates = [  JalaliDate(int(content[i].split(',')[0].split('/')[0]),
                         int(content[i].split(',')[0].split('/')[1]), 
                         int(content[i].split(',')[0].split('/')[2])).to_gregorian()   for i in range(len(content))]
    dates_2 = pd.to_datetime(dates)

    indexx = [float(content[i].split(',')[1]) for i in range(len(content))]
    df = pd.DataFrame(indexx, dates_2, columns=['indexx'])
    return df




holiday = 0
morning = 0
second_check_static = 0
each_iter =0 

while True:
    try:
        time_x = datetime.datetime.now()
        if(time_x > (datetime.datetime.combine(time_x, datetime.time(8,55)))) and (morning == 0) and\
                (time_x < (datetime.datetime.combine(time_x, datetime.time(12, 31)))):
            print('goood  morning sir! from docker hajm')
            each_iter=0
            telegram_msg_just_me('goood  morning sir! from docker hajm')
            m = 0
            m_range = 0
            cst = 100
            cst_range = 15
            AVEGRAGE = pd.read_excel('Average_vol_symbols.xlsx')
            AVEGRAGE = AVEGRAGE.drop(['Unnamed: 0', 'symbol'], axis=1)
            AVEGRAGE['Average_monthly_volume'][AVEGRAGE['Average_monthly_volume'] < 100e3] = 2e6
            AVEGRAGE['id'] = AVEGRAGE['id'].astype(str)
            static = Market_with_askbid()
            static['symbol'] = static.index
            static = pd.merge(static, AVEGRAGE, how='left', on='id')
            static.index = static['symbol']
            static['bidask'] = 0
            static['trend'] = 0
            static['avg_val_month'] = static['Average_monthly_volume'] * static['first_price']
            static['avg_val_month_to_5'] = static['Average_monthly_volume'] * static['first_price']/5
            static['avg_val_month_to_10'] = static['Average_monthly_volume'] * static['first_price']/10
            
            
            static['cst_amount'] = 15e10
            static['cst_amount_range'] = 3e10
            
            static['bidask'][static['min_allowed_price'] == static['bid_price'] ] = -1
            static['bidask'][static['max_allowed_price'] == static['ask_price'] ] = 1
            static['sumvalues'] = np.nan
            static['bidaskmin'] = np.nan
            static['bidaskmax'] = np.nan
            static['valueBQ_till_now'] = 0
            static['valueSQ_till_now'] = 0
            static['traded_value_in_queue']  = 0
            static['trend_range_min'] = np.nan
            static['trend_range_max'] = np.nan
            
            firsttime_trade_queue = (datetime.datetime.combine(datetime.datetime.now(),\
                                                               (time_x+datetime.timedelta(minutes=10)).time()))
            timelist_trade_queue = [firsttime_trade_queue]
            for i in range(40):
                firsttime_trade_queue += datetime.timedelta(minutes=15)
                timelist_trade_queue.append(firsttime_trade_queue)
            for i in range(cst):
                static['tr_' + str(i)] = np.nan
                static['bidask_'+str(i)]=np.nan
                static['deltaV_'+str(i)]=np.nan
            for i in range(cst_range):
                static['range_tr_'+str(i)] = np.nan
                static['range_deltaV_'+str(i)] = np.nan
                static['range_bidask_'+str(i)]=np.nan
                
                

            
            
            print('done!  docker hajmzz----')
            holiday = holiday_market()
            morning = 1
            telegram_msg_just_me('hajmlogs : morning is 1  and the holiday is ', holiday)
            for id in list(static['id'][static['Average_monthly_volume'].isna()]):
                try:
                    av = average_month(id)
                    static['Average_monthly_volume'][static['id'] ==id] = av
                except:
                    static['Average_monthly_volume'][static['id'] ==id] = 2e6
                    pass


        elif time_x > (datetime.datetime.combine(time_x, datetime.time(12,35))):
            oo = (datetime.datetime.combine(datetime.datetime.now(),datetime.time(23, 59)) -\
                  datetime.datetime.now()).seconds + datetime.timedelta(\
                      hours=8, minutes=30).seconds

            holiday = 0
            morning = 0
            second_check_static = 0
            telegram_msg_just_me('hajmlogs: miram tu khabe zemestooni yerooze...' + str(each_iter) )
            each_iter =0 
            time.sleep(oo)

        elif time_x > (datetime.datetime.combine(time_x, datetime.time(9,0))) and holiday == 0 and\
                time_x < (datetime.datetime.combine(time_x, datetime.time(12, 31))) and morning == 1:
            try:
                instance_time = datetime.datetime.now()
                m = m % cst
                m_range = m_range % cst_range
                all_data = Market_with_askbid()
                all_data['symbol'] = all_data.index  
                all_data = all_data.sort_index()
                static = static.sort_index()
                all_data['bidask'] = 0
                all_data['bidask'][all_data['min_allowed_price'] == all_data['bid_price'] ] = -1
                all_data['bidask'][all_data['max_allowed_price'] == all_data['ask_price'] ] = 1
                if all_data.shape[0] < 300:
                    continue

                
                
                new_shares = [share for share in list(all_data.symbol)if share not in list(static.index)]
                for symbol in new_shares:
                    static = static.append(all_data[all_data['symbol'] == symbol])
                    try:
                        idd = static['id'][static['symbol']==symbol].iloc[0]
                        static['Average_monthly_volume'][static['symbol']==symbol] = \
                        AVEGRAGE['Average_monthly_volume'][AVEGRAGE['id']==idd].iloc[0]
                    except:
                        static['Average_monthly_volume'][static['symbol']==symbol] = 2e6
                new_shares2 = [share for share in list (static.symbol) if share not in list(all_data.symbol)]
                for symbol in new_shares2:
                    static = static.drop(static[static['symbol'] == symbol].index)
                all_data = all_data.sort_index()
                static = static.sort_index()
                all_data['value'] = np.where( all_data['value'] <= static['value'] , static['value'],  all_data['value'] )

                
                static['bidask_'+str(m)] = all_data['bidask'].copy()
                static['tr_'+str(m)] = np.where(all_data['last_trade']>static['last_trade'],1,-1)
                static['tr_'+str(m)] = np.where(all_data['last_trade']==static['last_trade'],0,static['tr_'+str(m)])
                static['deltaV_'+str(m)] = all_data['value'] - static['value']
                static['lastret_'+str(m)] = all_data['last_ret'].copy()

                static['range_tr_'+str(m_range)] = np.where(all_data['last_trade']>static['last_trade'],1,-1)
                static['range_tr_'+str(m_range)] =  np.where(all_data['last_trade']==static['last_trade'],0,static['range_tr_'+str(m_range)])
                
                static['range_deltaV_'+str(m_range)] = all_data['value'] - static['value']
                static['range_bidask_'+str(m_range)]= all_data['bidask'].copy()      
                
                retcolz = [ col for col in static.columns if col.startswith('lastret_')]
                
                
                volcolz = [ col for col in static.columns if col.startswith('deltaV_')]
                static['sumvalues'] = static[volcolz].sum(axis=1)
                
                bidaskz = [ col for col in static.columns if col.startswith('bidask_')]
                static['bidaskmin'] = static[bidaskz].min(axis=1)
                static['bidaskmax'] = static[bidaskz].max(axis=1)
                static['threshold'] = static[['avg_val_month_to_5' , 'cst_amount']].max(axis=1)
                
                static['lastret_min'] = static[retcolz].min(axis=1)
                static['lastret_max'] = static[retcolz].max(axis=1)
                
                
                suspected_volumes = static[(static['sumvalues'] > static['threshold']) & (static['bidaskmin'] <=0)&\
                                          (static['bidaskmax'] >= 0)]
                
                if not suspected_volumes.empty:
                    string_suspect = '⚠️🔦حجم مشکوک در نماد(های): \n'
                    for symbol in list(suspected_volumes.index):
                        string_suspect += '#' + symbol + URL(all_data['id'][all_data['symbol']==symbol].iloc[0]) + \
                            ' به ارزش ' + readable(static['sumvalues'][static.index == symbol].iloc[0]) + '\n'
                    static.loc[(static.index).isin(suspected_volumes.index), volcolz] = 0
                    static.loc[(static.index).isin(suspected_volumes.index), bidaskz] = 0
                    telegram_msg((string_suspect))
                    
                trcolz_range = [ col for col in static.columns if col.startswith('range_tr_')]
                volcolz_range = [ col for col in static.columns if col.startswith('range_deltaV_')]
                bidaskz_range = [ col for col in static.columns if col.startswith('range_bidask_')]
                
                static['sumvalues_range'] = static[volcolz_range].sum(axis=1)

                static['trend_range_min'] = static[trcolz_range].min(axis=1)
                static['trend_range_max'] = static[trcolz_range].max(axis=1)
                static['trend_range_average'] = static[trcolz_range].mean(axis=1)   
                static['trend_range_sum'] = static[trcolz_range].sum(axis=1)
                
                
                static['bidaskmin_range'] = static[bidaskz_range].min(axis=1)
                static['bidaskmax_range'] = static[bidaskz_range].max(axis=1)
                
                static['threshold_range'] = static[['avg_val_month_to_10' , 'cst_amount_range']].max(axis=1)
                static['threshold_range'] = np.where( static['threshold_range'] > 5e10 , 5e10 ,static['threshold_range'])
                
                upranged = static[(static['sumvalues_range'] > static['threshold_range']) & \
                                   (static['trend_range_sum'] >= 3) &(static['Market_cap'] > 3e13)&\
                                  ( static['lastret_max'] -  static['lastret_min'] >= 0.01 ) & ( all_data['value'] > 1.5e10)]

                if not upranged.empty:
                    string_upranged = 'رنج مثبت در نماد(های)✅✅: \n'
                    for symbol in list(upranged.index):
                        string_upranged += '#' + symbol + URL(all_data['id'][all_data['symbol']==symbol].iloc[0]) + '\n'
                    static.loc[(static.index).isin(upranged.index), volcolz_range] = 0
                    static.loc[(static.index).isin(upranged.index), bidaskz_range] = 0
                    static.loc[(static.index).isin(upranged.index), trcolz_range] = np.nan
                    
                    telegram_msg((string_upranged))                   
                    

                downranged = static[(static['sumvalues_range'] > static['threshold_range']) \
                                  & (static['trend_range_sum'] <= -3) &(static['Market_cap'] > 3e13)&\
                                  ( static['lastret_max'] -  static['lastret_min'] >= 0.01 ) & ( all_data['value'] > 1e10)]

                if not downranged.empty:
                    string_downranged = 'رنج منفی در نماد(های) ❌: \n'
                    for symbol in list(downranged.index):
                        string_downranged += '#' + symbol + URL(all_data['id'][all_data['symbol']==symbol].iloc[0]) + '\n'
                    static.loc[(static.index).isin(downranged.index), volcolz_range] = 0
                    static.loc[(static.index).isin(downranged.index), bidaskz_range] = 0
                    static.loc[(static.index).isin(downranged.index), trcolz_range] = 0
                    
                    telegram_msg((string_downranged))                            
                        
                        
                        
                        
                static['traded_value_in_queue'] = np.where(static['bidask']  == 1 ,( all_data['value'] - static['value']), 0 )
                static['valueBQ_till_now'] += static['traded_value_in_queue'].copy()
                static['traded_value_in_queue'] = 0 
                static['traded_value_in_queue'] = np.where(static['bidask']  == -1 ,( all_data['value'] - static['value']), 0 )
                static['valueSQ_till_now'] += static['traded_value_in_queue'].copy()
                static['traded_value_in_queue'] = 0 
                m += 1
                m_range += 1
                static['last_trade'] = all_data['last_trade'].copy()
                static['value'] = all_data['value'].copy()
                static['bidask'] = all_data['bidask'].copy()
                each_iter += 1
                if instance_time >  timelist_trade_queue[0] :
                    print('inside the trade queue list---'+ str(datetime.datetime.now()) )
                    k=10
                    string = '#وضعیت_معاملات_در_صفهای_خرید_فروش  \n'  +\
                    convert_en_numbers(jdatetime.datetime.now().strftime("%A, %d %b %Y ساعت - %M: %H")) +'  🇮🇷 ⏰ ' +  "\n\n"
                    string  +=  '🔊ارزش معاملات در صف های خرید  :  ' + '\n <strong>' + readable(static['valueBQ_till_now'].sum()) +\
                    ' </strong> \n'+'ارزش معاملات در صف های فروش :  ' + '\n <strong>' +\
                    readable(static['valueSQ_till_now'].sum()) +'</strong> \n'+\
                    '🔷 🌝بیشترین ارزش معاملات در صف خرید در نماد های  :  '  + ' \n' 
                    top_BQ_values = static.sort_values('valueBQ_till_now', ascending = False).head(k)
                    top_SQ_values = static.sort_values('valueSQ_till_now', ascending = False).head(k)
                    for item in list(top_BQ_values.symbol):
                        string += '#' + item +  URL(all_data['id'][all_data['symbol'] == item].iloc[0]) +\
                        ' به ارزش : ' + readable(str(static['valueBQ_till_now'][static['symbol']==item].iloc[0])) + '\n'
                    string += '\n' +  ' 🌚🔶 بیشترین ارزش معاملات در صف فروش در نمادهای :  \n'
                    for item  in list(top_SQ_values.symbol):
                        string += '#' +item +URL(all_data['id'][all_data['symbol'] == item].iloc[0]) +\
                        ' به ارزش : ' + readable(str(static['valueSQ_till_now'][static['symbol']==item].iloc[0])) + '\n'
                    telegram_msg((string))
                    timelist_trade_queue.pop(0)
                        

                

            except (requests.exceptions.ReadTimeout, NameError, TypeError,RuntimeError) as e:
                time.sleep(5)
                print(e)
                continue

        else:
            print('tu hichkodooom nemiram')
            time.sleep(5)
            pass

    except Exception as e:
        print(e, '-- main error--')
        time.sleep(5)
        pass