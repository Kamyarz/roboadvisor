import re
import json
import requests
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings('ignore')
import os
import datetime
import jdatetime
import locale
import re
jdatetime.set_locale('fa_IR')
import time
import statistics
import json
from persiantools.jdatetime import JalaliDate
import datetime 
def holiday_market():
    static = Market_with_askbid()
    time.sleep(5)
    static2 = Market_with_askbid()
    boolz = static['bid_vol'].sum() == static2['bid_vol'].sum()
    if boolz:
        holiday = 1
    else:
        holiday=0
    return holiday
def market():
    url='http://www.tsetmc.com/tsev2/data/MarketWatchPlus.aspx'
    data = requests.get(url, timeout=10)
    content = data.content.decode('utf-8')

    parts = content.split('@')
    inst_price = parts[2].split(';')
    market = { }
    for item in inst_price:
        item=item.split(',')
        market[item[2]]= dict(id=item[0],ISIN=item[1],symbol=item[2],
                              name=item[3],first_price=item[5],close_price=item[6],
                              last_price=item[7],count=item[8],volume=item[9],
                              value=item[10],min_traded_price=item[11],
                              max_treaded_price=item[12],yesterday_price=item[13],
                              eps=item[14],base_volume=item[15],c2=item[16],
                              table_id=item[17],group_id=item[18],max_allowed_price=item[19],
                              min_allowed_price=item[20],type_of_symbol=item[22],
                              number_of_shares=item[21], Market_cap=int(item[21]) *int(item[6]) )
    return market
def indiv():
    count = 0
    while count <= 10:
        url = 'http://www.tsetmc.com/tsev2/data/ClientTypeAll.aspx'
        data = requests.get(url, timeout=10)
        content=data.content.decode('utf-8').split(";")
        if data.status_code != 200:
            count += 1
            time.sleep(1)
        elif data.status_code == 200:
            break
        elif count == 10:
            raise Exception('Loop Client!')
        else:
            time.sleep(1)
            pass
    all_data = pd.DataFrame(market()).T
    all_data = all_data[all_data['ISIN'].map(lambda x:not ( x.startswith('IRT')))]
    all_data = all_data[all_data['ISIN'].map(lambda x:not ( x.startswith('IRB')))]
    
    others = ['آ س پ' , 'جم پيلن', 'كي بي سي' , 'فن آوا' , 'انرژي3' , 'دتهران']
    for item in list(all_data.index):
        if item.isalpha() ==False:
            if item in others:
                continue
            all_data = all_data.drop(item, axis = 0)

    if True:
        all_data['num'] = [i for i in range(all_data.shape[0])]
        all_data = all_data.set_index('num')
    clienttype=[]
    for item in content:
        try:
            item=item.split(',')
            symbol = all_data[ all_data['id'] ==  (item[0]) ]['symbol'].iloc[0]
            clienttype.append(dict( id=item[0],Name = symbol,
                                       Individual_buy_count=int(item[1]),
                                          NonIndividual_buy_count=int(item[2]),
                                          Individual_buy_volume=int(item[3]),
                                          NonIndividual_buy_volume=int( item[4]) ,
                                          Individual_sell_count=int(item[5]),
                                          NonIndividual_sell_count=int(item[6]),
                                          Individual_sell_volume=int(item[7]),
                                          NonIndividual_sell_volume=int(item[8]),
                                         Value = float(all_data[all_data['symbol'] == symbol]['value'])))
        except:
            continue
    clients = pd.DataFrame(clienttype)
    clients.Name = clients.Name.map(lambda x: convert_ar_characters(x) )
    clients['VAL_hoghooghi_SELL'] = clients['Value'] * clients['NonIndividual_sell_volume'].astype(float) /\
    (clients['Individual_sell_volume'].astype(float) +clients['NonIndividual_sell_volume'].astype(float))

    clients['VAL_haghighi_BUY'] = clients['Value'] * clients['Individual_buy_volume'].astype(float) /\
    (clients['Individual_buy_volume'].astype(float) +clients['NonIndividual_buy_volume'].astype(float))
    clients['VAL_haghighi_SELL'] = clients['Value'] * clients['Individual_sell_volume'].astype(float) /\
    (clients['Individual_sell_volume'].astype(float) +clients['NonIndividual_sell_volume'].astype(float))
    clients['percapita_buy'] = clients['VAL_haghighi_BUY'] / clients['Individual_buy_count']
    clients['percapita_sell'] = clients['VAL_haghighi_SELL'] / clients['Individual_sell_count']
    clients['power'] = clients['percapita_buy'] / clients['percapita_sell']
    clients['VAL_net_haghigh'] = clients['VAL_haghighi_BUY'] - clients['VAL_haghighi_SELL']

    return clients 
def Market_with_askbid():
    count = 0 
    while count<15:
        url = 'http://www.tsetmc.com/tsev2/data/MarketWatchPlus.aspx?h=0&r=0'
        data = requests.get(url, timeout=9)
        content = data.content.decode('utf-8')
        parts = content.split('@')
        if data.status_code != 200 or len(content.split('@')[2])<400:
            count+=1
            time.sleep(1)
        if count ==15:
            raise Exception('ohoh')
        if data.status_code == 200 and len(content.split('@')[2]) > 400:
            break
    parts = content.split('@')
    inst_price = parts[2].split(';')
    market_me = {}
    # Add the Trade and other stuff to dataframe--------
    for item in inst_price:
        item=item.split(',')
        market_me[item[0]]= dict(id=item[0],ISIN=item[1],symbol=item[2],
                              name=item[3],first_price=float(item[5]),close_price=float(item[6]),
                              last_trade=float(item[7]),count=item[8],volume=float(item[9]),
                              value=float(item[10]),min_traded_price=float(item[11]),
                              max_treaded_price=float(item[12]),yesterday_price=int(item[13]),
                              table_id=item[17],group_id=item[18],max_allowed_price=float(item[19]),
                              min_allowed_price=float(item[20]),last_ret = (float(item[7]) - float(item[13]))/float(item[13]),
                                 ret = (float(item[6]) - float(item[13]))/float(item[13]),
                                number_of_shares=float(item[21]), Market_cap=int(item[21]) *int(item[6]))
    # Add the Ask-Bid price Vol tu dataframe --------
    for item in parts[3].split(';'):
        try:
            item=item.split(',')
            if item[1] == '1':
                market_me[item[0]]['ask_price'.format(item[1])]=  float(item[4])
                market_me[item[0]]['ask_vol'.format(item[1])]=  float(item[6])
                
                market_me[item[0]]['bid_price'.format(item[1])]=  float(item[5])
                market_me[item[0]]['bid_vol'.format(item[1])]=  float(item[7])

        except:
            pass
    df = pd.DataFrame(market_me).T
    df = df[df['ISIN'].map(lambda x:not ( x.startswith('IRT')))]
    df = df[df['ISIN'].map(lambda x: not( x.startswith('IRB')))]
    df = df[df['symbol'].map(lambda x: x.isalpha())]
    df.symbol = df.symbol.map(lambda x: convert_ar_characters(x) )
    df = df.set_index('symbol')
    return df
def average_month(id):
    counter = 0
    while counter < 10:
        url = 'http://members.tsetmc.com/tsev2/data/InstTradeHistory.aspx?i={}&Top=20&A=1'.format(
            id)
        data = requests.get(url, timeout=8, verify=False)
        content = data.content.decode('utf-8').split(';')
        if data.status_code == 200 and len(content) > 5:
            break
        else:
            counter += 1
    hist_vol = []
    for i in range(len(content) - 1):
        hist_vol.append(float(content[i].split('@')[-2]))
    mean = statistics.mean(hist_vol)
    return mean
def readable(n):
    human_readable =''
    if float(n) < 0 : 
        human_readable += '-'
    n = abs(float(n))
    if n >= 1e7 and n<= 1e10 :
        round_number = n/1e7
        human_readable += '{:,.1f}{}'.format(round_number,   ' میلیون تومان ')
    elif n>1e10:
        round_number = n/1e10
        
        human_readable += '{:,.1f}{}'.format(round_number, ' میلیارد تومان ')
    else:
        round_number = n/10
        human_readable += '{:,.1f}{}'.format(round_number, 'تومان')
        
    return convert_en_numbers(human_readable)
def readable_tedad(n):
    human_readable =''
    if int(n) < 0 : 
        human_readable += '-'
    n = abs(int(n))
    if n >= 1e6 and n<= 1e9 :
        round_number = n/1e6
        human_readable += '{:,.1f}{}'.format(round_number,   ' میلیون ')
    elif n>1e9:
        round_number = n/1e9
        
        human_readable += '{:,.1f}{}'.format(round_number, ' میلیارد ')
    else:
        human_readable += '{:,}'.format(n)
    return convert_en_numbers(human_readable)
marketwatch_id= '-1001470501669'
my_id = '80911655'
def telegram_msg(msg , chat_id = "-1001470501669"):
    headers = {'Content-type': 'application/json'}
    payload = {"bot-name" : "hermes" , 
              "chat-id" : chat_id,
              "message" : msg,
              "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                      headers = headers,
                      data=json.dumps(payload))
    payload = {"bot-name" : "hermes" , 
              "chat-id" : '-1001453076404',
              "message" : msg,
              "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                      headers = headers,
                      data=json.dumps(payload))
    payload = {"bot-name" : "hermes" , 
          "chat-id" : '-1001279557836',
          "message" : msg,
          "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                  headers = headers,
                  data=json.dumps(payload))
    payload = {"bot-name" : "hermes" , 
          "chat-id" : '-1001437707813',
          "message" : msg,
          "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                  headers = headers,
                  data=json.dumps(payload))
    
    
    if r.status_code == 200:
        return True
    else: return False
def URL(id):
    if type(id) == str:
        URLp = 'http://www.tsetmc.com/Loader.aspx?ParTree=151311&i={}'.format(id)
    else: 
        id = str(id)
        URLp = 'http://www.tsetmc.com/Loader.aspx?ParTree=151311&i={}'.format(id)
    return '<a href="{}"> -{}</a>'.format(URLp, '(TSETMC)')
def telegram_msg_just_me(msg , chat_id = "80911655"):
    headers = {'Content-type': 'application/json'}
    payload = {"bot-name" : "hermes" , 
              "chat-id" : chat_id,
              "message" : msg,
              "parse-mode" : "html"}
    r = requests.post('http://178.62.251.62:8891/send-message', 
                      headers = headers,
                      data=json.dumps(payload))
def convert_en_numbers(input_str):
    """
    Converts English numbers to Persian numbers
    :param input_str: String contains English numbers
    :return: New string with Persian numbers
    """
    mapping = {
        '0': '۰',
        '1': '۱',
        '2': '۲',
        '3': '۳',
        '4': '۴',
        '5': '۵',
        '6': '۶',
        '7': '۷',
        '8': '۸',
        '9': '۹',
        '.': '.',
    }
    return _multiple_replace(mapping, input_str)
def convert_en_characters(input_str):
    """
        Assumes that characters written with standard persian keyboard
        not windows arabic layout
    :param input_str: String contains English chars
    :return: New string with related characters on Persian standard keyboard
    """
    mapping = {
        'q': 'ض',
        'w': 'ص',
        'e': 'ث',
        'r': 'ق',
        't': 'ف',
        'y': 'غ',
        'u': 'ع',
        'i': 'ه',
        'o': 'خ',
        'p': 'ح',
        '[': 'ج',
        ']': 'چ',
        'a': 'ش',
        's': 'س',
        'd': 'ی',
        'f': 'ب',
        'g': 'ل',
        'h': 'ا',
        'j': 'ت',
        'k': 'ن',
        'l': 'م',
        ';': 'ک',
        "'": 'گ',
        'z': 'ظ',
        'x': 'ط',
        'c': 'ز',
        'v': 'ر',
        'b': 'ذ',
        'n': 'د',
        'm': 'پ',
        ',': 'و',
        '?': '؟',
    }
    return _multiple_replace(mapping, input_str)
def convert_ar_numbers(input_str):
    """
    Converts Arabic numbers to Persian numbers
    :param input_str: String contains Arabic numbers
    :return: New str and replaces arabic number with persian numbers
    """
    mapping = {
        '١': '۱',  # Arabic 1 is 0x661 and Persian one is 0x6f1
        '٢': '۲',  # More info https://goo.gl/SPiBtn
        '٣': '۳',
        '٤': '۴',
        '٥': '۵',
        '٦': '۶',
        '٧': '۷',
        '٨': '۸',
        '٩': '۹',
        '٠': '۰',
    }
    return _multiple_replace(mapping, input_str)
def convert_fa_numbers(input_str):
    """
    This function convert Persian numbers to English numbers.
    
    Keyword arguments:
    input_str -- It should be string
    Returns: English numbers
    """
    mapping = {
        '۰': '0',
        '۱': '1',
        '۲': '2',
        '۳': '3',
        '۴': '4',
        '۵': '5',
        '۶': '6',
        '۷': '7',
        '۸': '8',
        '۹': '9',
        '.': '.',
    }
    return _multiple_replace(mapping, input_str)
def convert_ar_characters(input_str):
    """
    Converts Arabic chars to related Persian unicode char
    :param input_str: String contains Arabic chars
    :return: New str with converted arabic chars
    """
    mapping = {
        'ك': 'ک',
        'دِ': 'د',
        'بِ': 'ب',
        'زِ': 'ز',
        'ذِ': 'ذ',
        'شِ': 'ش',
        'سِ': 'س',
        'ى': 'ی',
        'ي': 'ی'
    }
    return _multiple_replace(mapping, input_str)
def _multiple_replace(mapping, text):
    """
    Internal function for replace all mapping keys for a input string
    :param mapping: replacing mapping keys
    :param text: user input string
    :return: New string with converted mapping keys to values
    """
    pattern = "|".join(map(re.escape, mapping.keys()))
    return re.sub(pattern, lambda m: mapping[m.group()], str(text))
def main_index():
    from persiantools.jdatetime import JalaliDate
    import requests
    import pandas as pd
    url='http://www.tsetmc.com/tsev2/chart/data/Index.aspx?i=32097828799138957&t=value'
    data=requests.get(url)
    content= data.content.decode('utf-8').split(';')
    dates = [  JalaliDate(int(content[i].split(',')[0].split('/')[0]),
                         int(content[i].split(',')[0].split('/')[1]), 
                         int(content[i].split(',')[0].split('/')[2])).to_gregorian()   for i in range(len(content))]
    dates_2 = pd.to_datetime(dates)

    indexx = [float(content[i].split(',')[1]) for i in range(len(content))]
    df = pd.DataFrame(indexx, dates_2, columns=['indexx'])
    return df  
    
    
    
    
    
    

holiday = 0
morning = 0
second_check_static = 0
m=0
each_iter=0
while True:
    try:
        time_sadegh = datetime.datetime.now()
        if time_sadegh > (datetime.datetime.combine(time_sadegh , datetime.time(9,3))) and  \
        time_sadegh < (datetime.datetime.combine(time_sadegh , datetime.time(12,30))) and \
        morning==0:
            each_iter=0
            telegram_msg_just_me('starting the morning ctoc')
            cst = 250
            main = indiv()
            main.dropna(inplace=True)
            main.reset_index(drop=True,inplace=True)
            morning=1
            m=0
            for i in range(cst):
                main['Val_' + str(i)] = 0
                main['I_Buy_diffval_' + str(i)] = 0
                main['I_sell_diffval_' + str(i)] = 0
                main['N_sell_diffval_' + str(i)]= 0
                main['Power_' + str(i)] = np.nan
                main['Sb_' + str(i)] = np.nan
                main['Ss_' + str(i)] = np.nan
                
                
                
            main['sum_value'] = 0
            main['sum_I_buy_value'] = 0
            main['sum_I_sell_value'] = 0
            main['sum_N_sell_value']=0

            
            holiday = holiday_market()
            telegram_msg_just_me('end of the first section from docker --ctoc! full ready')
            telegram_msg_just_me('holiday( for CtoC)  is : '  + str(holiday))
        
                
        elif time_sadegh > (datetime.datetime.combine(time_sadegh , datetime.time(12,35))):
            holiday = 0
            morning=0
            second_check_static=0
            oo = (datetime.datetime.combine(datetime.datetime.now() , datetime.time(23,59)) -  \
                  datetime.datetime.now()).seconds + datetime.timedelta(hours=9, minutes=0).seconds
            telegram_msg_just_me('miram tu khabe zemestoooni----ctoc  ' + str(each_iter))
            each_iter = 0
            time.sleep(oo)



        elif time_sadegh> (datetime.datetime.combine(time_sadegh, datetime.time(9,4))) and\
        holiday ==0 and time_sadegh < (datetime.datetime.combine(time_sadegh, datetime.time(12,31)))and\
        morning==1:
            try:
                m = m%cst
                start = datetime.datetime.now()
                new = indiv()
                each_iter += 1
                
                new.dropna(inplace=True)
                new = new.sort_values('Name')
                main = main.sort_values('Name')
                new.reset_index(drop=True,inplace=True)
                main.reset_index(drop=True,inplace=True)
                new_shares = [share for share in list (new.Name) if share not in list(main.Name)]
                for symbol in new_shares:
                    main = main.append(new[new['Name'] == symbol])
                    for i in range(cst):
                        main['Val_' + str(i)][new['Name'] == symbol] = 0
                        main['I_Buy_diffval_' + str(i)][new['Name'] == symbol]  = 0
                        main['N_sell_diffval_' + str(i)][new['Name'] == symbol] = 0
                    main['sum_I_buy_value'][ main['Name'] ==symbol] = 0
                    main['sum_N_sell_value'][ main['Name'] ==symbol] =0
                new_shares2 = [share for share in list (main.Name) if share not in list(new.Name)]
                for symbol in new_shares2:
                    main = main.drop(main[main['Name'] == symbol].index)
                    main.reset_index(drop=True,inplace=True)
                new = new.sort_values('Name')
                main = main.sort_values('Name')
                new.reset_index(drop=True,inplace=True)
                main.reset_index(drop=True,inplace=True)
                new['Value'] = np.where( new['Value'] <= main['Value'] , main['Value'],  new['Value'] )

                main['Val_' + str(m)] = new['Value'] - main['Value']
                main['I_Buy_diffval_' + str(m)] = new['VAL_haghighi_BUY'] - main['VAL_haghighi_BUY']
                main['I_sell_diffval_' + str(m)] = new['VAL_haghighi_SELL'] - main['VAL_haghighi_SELL']
                main['N_sell_diffval_' + str(m)] = new['VAL_hoghooghi_SELL'] - main['VAL_hoghooghi_SELL']
                main['Power_' + str(m)] = new['power'] 
                main['Sb_' + str(m)] = new['percapita_buy']
                main['Ss_' + str(m)] = new['percapita_sell']




                main['sum_value'] = 0
                main['sum_I_buy_value'] = 0
                main['sum_I_sell_value'] = 0
                main['sum_N_sell_value']=0


                valcol = [ col for col in main.columns if col.startswith('Val_')]
                IBuycol = [ col for col in main.columns if col.startswith('I_Buy_diffval_')]
                ISellcol = [ col for col in main.columns if col.startswith('I_sell_diffval_')]
                NSellcol = [ col for col in main.columns if col.startswith('N_sell_diffval_')]
                
                
                main['sum_value'] =  main[valcol].sum(axis=1)
                main['sum_I_buy_value'] =  main[IBuycol].sum(axis=1)
                main['sum_I_sell_value'] =  main[ISellcol].sum(axis=1)
                main['sum_N_sell_value'] =  main[NSellcol].sum(axis=1)
                
                code_to_code = main[(main['sum_I_buy_value'] >6e10) & (main['sum_I_buy_value'] / main['sum_value'] > 0.85) &\
                                    (main['sum_N_sell_value'] / main['sum_value'] > 0.85) ]
                code_to_code_colz = [col for col in main.columns if col.startswith('I_Buy_diffval_') or\
                                     col.startswith('I_sell_diffval_') or\
                                     col.startswith('N_sell_diffval_') or col.startswith('Val_')] 

                if code_to_code.empty == False:
                    msg = '#'+ 'احتمال_کد_به_کد' + '\n'
                    msg +=  'فروش  سنگین حقوقی به حقیقی  در نماد(های):🎯🕰️👀  \n'
                    for item in list(code_to_code.Name):
                        msg += '#' + item + URL(new['id'][new['Name'] == item].iloc[0]) + ' به ارزش: ' +\
                        readable(main['sum_I_buy_value'][main['Name'] == item].iloc[0])+'\n'
                    main.loc[code_to_code.index , code_to_code_colz] = 0

                    telegram_msg(msg)



                    
                    
                main['Value'] = new['Value'].copy()
                main['VAL_haghighi_BUY'] = new['VAL_haghighi_BUY'].copy()
                main['VAL_hoghooghi_SELL'] = new['VAL_hoghooghi_SELL'].copy()
                main['VAL_haghighi_SELL'] = new['VAL_haghighi_SELL'].copy()
                main['power'] = new['power'].copy()
                
                



                power_selected = [col for col in main.columns if col.startswith('Power_')]

                main['max_power'] = main[power_selected].max(axis=1)
                main['min_power'] = main[power_selected].min(axis=1)
                
                powerz = main[(main['max_power'] > 1.2* main['min_power']) &( new['percapita_buy'] > 1e7)&( new['power'] > 0.1) \
                              (new['percapita_sell'] > 1e7) &(new['VAL_haghighi_BUY'] > 1e10)& ( main['min_power'] != 0)]
                
                if powerz.empty == False:
                    msg = '#'+ 'افزایش_قدرت_خریداران_حقیقی♠️🔋' + '\n'
                    msg += ' در نمادهای: \n' 
                    for item in list(powerz.Name):
                        msg += '#' + item + URL(new['id'][new['Name'] == item].iloc[0])+ '\n'
                    main.loc[powerz.index , power_selected]= 0

                    
                    telegram_msg(msg)
    
                    
                    
                sb_selected = [col for col in main.columns if col.startswith('Sb_')]
                main['max_sb'] = main[sb_selected].max(axis=1)
                main['min_sb'] = main[sb_selected].min(axis=1)
                sbz = main[(main['max_sb'] > 1.2* main['min_sb']) &(new['Value']>2e10) \
                           &( new['percapita_buy'] > 2e7)&(new['percapita_sell'] > 1e7) \
                           &(new['VAL_haghighi_BUY'] > 1e10) & (main['min_sb'] != 0) &( new['power'] > 0.1) ]
                if sbz.empty == False:
                    msg = '#'+ 'افزایش_سرانه_خریداران_حقیقی🎩🟢' + '\n'
                    msg += ' در نمادهای : \n ' 
                    for item in list(sbz.Name):
                        msg += '#' + item + URL(new['id'][new['Name'] == item].iloc[0])+ '\n'
                    main.loc[sbz.index, sb_selected] = 0

                    
            
                    telegram_msg(msg)
                

                ss_selected = [col for col in main.columns if col.startswith('Ss_')]
                main['max_ss'] = main[ss_selected].max(axis=1)
                main['min_ss'] = main[ss_selected].min(axis=1)
                ssz = main[(main['max_ss'] > 1.2* main['min_ss']) &(new['Value']>4e10) & (main['min_ss'] != 0)\
                          &( new['percapita_buy'] > 2e7)&(new['percapita_sell'] > 2e7) &( new['power'] > 0.1) &(new['VAL_haghighi_BUY'] > 1e10) ]
                if ssz.empty == False:
                    msg = '#'+ 'افزایش_سرانه_فروشندگان_حقیقی♨️♨️' + '\n'
                    msg += ' در نمادهای : \n' 
                    for item in list(ssz.Name):
                        msg += '#' + item + URL(new['id'][new['Name'] == item].iloc[0])+ '\n'
                    main.loc[ssz.index, ss_selected] = 0

                    telegram_msg(msg)
                
                
                m += 1
                    
            except:
                time.sleep(5)
                pass
        else:
            time.sleep(5)
    except:
        time.sleep(5)
        pass

    